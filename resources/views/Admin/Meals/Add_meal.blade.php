@extends('index')
@section('content')

<div class="bs-example">
	<ul class="nav nav-tabs">
        <li class="active" id="meal"><a href="#">Add Meal</a></li>
        <li><a  id="tea">Add Tea</a></li>
	</ul>
</div>
    <div class="meal" style="margin-top:4%; background-color:white; padding-left:2%; padding-right:2%; padding:bottom:3%">
    <div class="row" style="background-color:white">
        <div class="col-sm-12">
            <h4>Add Meal</h4><hr>
        <form action="{{ route('meal.store') }}" method="POST" enctype="multipart/form-data" id="Add_meal_form">
                @csrf
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="meal">Select Date</label>
                    <input type="date" class="form-control{{ $errors->has('date') ? 'is-invalid' : '' }}" name="date" id="date" value="{{ old('date') }}">
                    @if ($errors->has('date'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="time">Meal Time</label>
                    <select class="form-control{{ $errors->has('time') ? 'is-invalid' : '' }}" name="time" id="time" value="{{ old('time') }}">
                            <option value=""></option>
                            <option value="breakfast">Breakfast</option>
                            <option value="lunch">Lunch</option>
                            <option value="dinner">Dinner</option>
                        </select>
                        @if ($errors->has('time'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('time') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="">Select Dish</label>
                    <select multiple class="chosen-select form-control" name="dish_name[]" required>
                                    @foreach ($dish as $item)
                                <option value="{{ $item->dish_name }}">{{ $item->dish_name }}</option>
                                     @endforeach
                            </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="rotti">Rotti Available?</label>
                            <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="rotti" value="1" checked>
                                <label class="custom-control-label">Yes</label>
                                <input type="radio" class="custom-control-input" name="rotti" value="0" style="margin-left:18%">
                                <label class="custom-control-label">No</label>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="margin-top:1%">
                        <button class="btn-success btn col-md pull-right" type="submit" name="add_meal">Add Meal</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
{{-- Tea Section --}}
    <div class="tea" style="display:none" >
            <div class="row" style="margin-top:4%; background-color:white" >

                <div class="col-sm-12">
                        <h4>Add Tea</h4><hr>
                <form action="{{ route('meal.store') }}" method="POST" enctype="multipart/form-data" id="Add_tea_form">
                        @CSRF
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="meal">Select Date</label>
                            <input type="date" class="form-control{{ $errors->has('date') ? 'is-invalid' : '' }}" name="date" id="date" value="{{ old('date') }}">
                            @if ($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                            @endif
                            </div>
                            <input type="hidden" name="tea" value="1">
                            <div class="form-group col-md-6">
                                <label for="tea_time">Tea Time</label>
                            <select class="form-control{{ $errors->has('time') ? 'is-invalid' : '' }}" name="time" id="time">
                                    <option value=" "></option>
                                    <option value="breakfast">Breakfast</option>
                                    <option value="lunch">Lunch</option>
                                    <option value="dinner">Dinner</option>
                                    <option value="lunch">10:00 - 12:00</option>
                                    <option value="dinner">4:00 - 6:00</option>
                                    <option value="dinner">12:00 - 6:00</option>
                                </select>
                                @if ( $errors->has('time') )
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row" >
                            <div class="form-group col-md-6" >
                                <label for="">Select Dish</label><br>
                            <select multiple class="chosen-select form-control" name="dish_name[]" required>
                                         @foreach ($product as $prd)
                                            <option value="{{ $prd['prd_name'] }}">{{ $prd['prd_name'] }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="col-sm-12" style="margin-top:1%">
                                <button class="btn-success btn col-md pull-right" type="submit" name="add_tea" value="add_tea">Add Tea</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
            @push('css')

            <style>
                .chosen-container{
                    width: 100% !important;
                }
            </style>
            @endpush
@stop
@section('footer_scripts')
<script src="js/select2.full.js"></script>
<!-- InputMask -->
<script src="{{ asset('js/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.extensions.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('js/icheck.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="{{ asset('js/chosen.jquery.js') }}"></script>

<script>

$(document).ready(function(){
    var $chosen = $('.chosen-select').chosen({
  max_selected_options: 6
});

$chosen.change(function () {
  var $this = $(this);
  var chosen = $this.data('chosen');
  var search = chosen.search_container.find('input[type="text"]');

  search.prop('disabled', $this.val() !== null);

  if (chosen.active_field) {
    search.focus();
  }
});
    $('#meal').click(function() {
        $('.tea').hide();
      $('.meal').show();
    });
    $('#tea').click(function() {
        $('.meal').hide();
      $('.tea').show();
    });

});
</script>
@endsection
