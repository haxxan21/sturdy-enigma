@extends('index')
@section('content')
<div class="row">
    <div class="col-xs-4 ">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search by Dish Name" onkeyup="dish_name()" id="myInput">
            <div class="input-group-btn search-panel">
                <button class="btn btn-success" style="border:none">
                    <span id="search_concept" style="color:white">Search By Name</span>
                </button>
            </div>
            <span class="input-group-btn">
                <i class="far fa-search"></i>
            </span>
        </div>
    </div>
        <div class="col-xs-4 ">
            <div class="input-group">
                <select class="form-control" onChange="dish_time()" id="dish_time">
                    <option value="">-- Select by Time --</option>
                    <option value="breakfast">Breakfast</option>
                    <option value="lunch">Lunch</option>
                    <option value="dinner">Dinner</option>
                </select>
                <div class="input-group-btn search-panel">
                    <button class="btn btn-warning" style="border:none">
                        <span id="search_concept" style="color:white">Search By Time</span>
                    </button>
                </div>
                <span class="input-group-btn">
                    <i class="far fa-search"></i>
                </span>
        </div>
    </div>
        <div class="col-xs-4">
                <div class="input-group">
                    <input type="date" class="form-control" onChange="dish_date()" id="dish_date">
                    <div class="input-group-btn search-panel">
                        <button class="btn btn-primary" style="border:none">
                            <span id="search_concept" style="color:white">Search By Date</span>
                        </button>
                    </div>
                    <span class="input-group-btn">
                        <i class="far fa-search"></i>
                    </span>
        </div>
    </div>

    </div>
    <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="">Select Date</label>
                    <div id="reportrange" class="pull-right form-control" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                      <span></span> <b class="caret"></b>
                    </div>
                  </div>
            </div>
            <div class="col-sm-6" style="margin-top:3%">
                <a href="{{ route('export.meal') }}"><button class="btn-primary btn btn-xs pull-right" style="margin-left:4%">Export to PDF</button></a>
                <a href="{{ route('export.excel') }}"><button class="btn-success btn btn-xs pull-right">Export to Excel</button></a>
            </div>
    </div>
    <div class="row" style="margin-top:3%; display:none" id="ajax">
        <div class="col-sm-12" style="background-color:white">
            <hr>
            <h4><strong>Choose Date to View Meal Schedule</strong></h4>
            <hr>
            <table class="table">
                <thead>
                    <th>No</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Dish Name</th>
                    <th>Rotti Available</th>
                </thead>
                <tbody id="tbody">
                    <tr>
                        <td>--  Please Select Dates  --</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top:3%;" >
        <div class="col-sm-12" style="background-color:white">
           <hr>
            <h4><strong>View Meal Schedule History</strong></h4>
            <hr>
            <table class="table" id="myTable" style="margin-bottom:3%" >
                <thead>
                    <th>No.</th>
                    <th>Meal Date</th>
                    <th>Meal time</th>
                    <th>Dish Name</th>
                    <th>Rotti Available</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($data as $info)
                    @if($info->tea == Null && !empty($info->dish_name))
                    <tr>
                    <td class="counterCell"></td>
                    <td>{{ $info->date }}</td>
                    <td>{{ $info->time }}</td>
                    <td>{{ $info->dish_name }}</td>
                    <td>@if($info->rotti == 1) Available @else Not Available @endif</td>
                    <td><form action="{{ route('meal.destroy',$info->id) }}" method="post" data-confirm="Are you sure you want to delete this meal schedule?">
                        @CSRF
                        @method('DELETE')
                        <Button class="btn btn-danger btn-xs pull-left" type="submit" >Delete</Button>
                    </form><a href="{{ route('meal.edit',$info->id) }}" style="margin-left:4%"><button class="btn btn-primary btn-xs pull-left">Edit</button></a>
                </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
            {{ $data->links() }}
        </div>
    </div>

<script>
    function cleanTable(){
  for(var i = 0; i<$('table tbody tr').length; i++){
      for(var j = 0; j<$('table tbody tr').length; j++){
      if($('table tbody tr').eq(i).html() == $('table tbody tr').eq(j).html() && i != j){
        $('table tbody tr').eq(j).remove();
      }
    }
  }
}
var start = null;
var end = null;
$('#reportrange span').bind('DOMSubtreeModified', function(e){
    start = $('input[name=daterangepicker_start]').val();
    end = $('input[name=daterangepicker_end]').val();
    if(typeof start !== 'undefined' && typeof end !== 'undefined'){
    $('#tbody').empty();
    var url = '/date'
    var url2 = '/fetch'
    $.get(url + '/' + start + '/' + end + url2, function(data){
    //   console.log(data.meals);
        for (var i = 0; i < data.meals.length; i++) {
                    var Dates = data.meals[i].Dates;
                    var Time = data.meals[i].Time;
                    var Dish = data.meals[i].Dish;
                    var Rotti_Available = data.meals[i].Rotti_Available;
         var meals = '<tr><td>' + i + '</td><td>' + Dates + '</td><td>' + Time + '</td><td>' + Dish + '</td><td>' + Rotti_Available + '</td></tr>'
         $("#tbody tr").each(function(){
            var first = $(this).find("td:first").text();
            console.log(first)
            });
                    $('#tbody').append(meals);
                    cleanTable();
                    $('#ajax').show();
                }
                // $('#tbody').empty();
    })
    }

});

function dish_name() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[3];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function dish_time() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("dish_time");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function dish_date() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("dish_date");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
$(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
    	e.stopImmediatePropagation();
      e.preventDefault();
		}
});

</script>
@endsection
