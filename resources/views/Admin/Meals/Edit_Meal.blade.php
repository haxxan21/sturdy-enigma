@extends('index')
@section('content')
<div class="meal">
        <div class="row">
            <div class="col-sm-12" style="background-color:white">
                <h4>Add Meal</h4><hr>
            <form action="{{ route('meal.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="meal">Select Date</label>
                        <input type="date" class="form-control" name="date" value="{{ $data->date }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="time">Meal Time</label>
                            <select class="form-control" name="time" id="1">
                            {{-- <option value="{{ $data->time }}" selected>{{ $data->time }}</option> --}}
                            @if($data->time == 'breakfast')
                                <option value="breakfast" selected>Breakfast</option>
                                <option value="lunch">Lunch</option>
                                <option value="dinner">Dinner</option>
                            @elseif($data->time == 'lunch')
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch" selected>Lunch</option>
                                <option value="dinner">Dinner</option>
                            @elseif($data->time == 'dinner')
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch">Lunch</option>
                                <option value="dinner" selected>Dinner</option>
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="">Select Dish</label>
                                <select multiple class="chosen-select form-control" name="dish_name[]">
                                <option value="{{ $data->dish_name }}" selected>{{ $data->dish_name }}</option>
                                        @foreach ($dish as $item)
                                    <option value="{{ $item->dish_name }}">{{ $item->dish_name }}</option>
                                         @endforeach
                                </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="rotti">Rotti Available?</label>
                                <div class="custom-control custom-radio">
                                    @if($data->rotti == 1)
                                     <input type="radio" class="custom-control-input" name="rotti" value="1" checked>
                                    <label class="custom-control-label">Yes</label>
                                    <input type="radio" class="custom-control-input" name="rotti" value="0" style="margin-left:18%">
                                    <label class="custom-control-label">No</label>
                                    @elseif($data->rotti == 0)
                                    <input type="radio" class="custom-control-input" name="rotti" value="1">
                                    <label class="custom-control-label">Yes</label>
                                    <input type="radio" class="custom-control-input" name="rotti" value="0" style="margin-left:18%" checked>
                                    <label class="custom-control-label">No</label>
                                    @endif
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6" style="margin-top:1%">
                            <button class="btn-success btn col-md" type="submit" name="update_meal">Update Meal Schedule</button>
                        </div>
                        <div class="col-sm-6" style="margin-top:1%">
                            <button type="submit" class="btn-primary btn pull-right" name="add_meal" value="add_meal" title="ADD SAME MEAL FOR ANOTHER DAY">Add Meal Schedule</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
        @stop
        @section('footer_scripts')
        <script src="js/select2.full.min.js"></script>
        <!-- InputMask -->
        <script src="{{ asset('js/jquery.inputmask.js') }}"></script>
        <script src="{{ asset('js/jquery.inputmask.date.extensions.js') }}"></script>
        <script src="{{ asset('js/jquery.inputmask.extensions.js') }}"></script>
        <!-- bootstrap datepicker -->
        <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
        <!-- iCheck 1.0.1 -->
        <script src="{{ asset('js/icheck.min.js') }}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                <script src="{{ asset('js/chosen.jquery.js') }}"></script>

        <script>

        $(document).ready(function(){
            var $chosen = $('.chosen-select').chosen({
          max_selected_options: 6
        });

        $chosen.change(function () {
          var $this = $(this);
          var chosen = $this.data('chosen');
          var search = chosen.search_container.find('input[type="text"]');

          search.prop('disabled', $this.val() !== null);

          if (chosen.active_field) {
            search.focus();
          }
        });
            $('#meal').click(function() {
                $('.tea').hide();
              $('.meal').show();
            });
            $('#tea').click(function() {
                $('.meal').hide();
              $('.tea').show();
            });

        });
        </script>
        @endsection
