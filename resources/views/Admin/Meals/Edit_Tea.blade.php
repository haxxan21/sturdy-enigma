@extends('index')
@section('content')

<div class="tea">
        <div class="row">

            <div class="col-sm-12" style="background-color:white">
                    <h4>Add Tea</h4><hr>
            <form action="{{ route('tea.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                    @CSRF
                    @method("PATCH")
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="meal">Select Date</label>
                            <input type="date" class="form-control" name="date" value="{{ $data->date }}">
                        </div>
                        <input type="hidden" name="tea" value="1">
                        <div class="form-group col-md-6">
                            <label for="tea_time">Tea Time</label>
                            <select class="form-control" name="time" id="1">
                                <option value=" "></option>
                            @if($data->time == 'breakfast')
                                <option value="breakfast" selected>Breakfast</option>
                                <option value="lunch" >Lunch</option>
                                <option value="dinner" >Dinner</option>
                                <option value="10:00-12:00">10:00 - 12:00</option>
                                <option value="4:00-6:00">4:00 - 6:00</option>
                                <option value="12:00-6:00">12:00 - 6:00</option>
                            @elseif($data->time == 'lunch')
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch" selected>Lunch</option>
                                <option value="dinner" >Dinner</option>
                                <option value="10:00-12:00">10:00 - 12:00</option>
                                <option value="4:00-6:00">4:00 - 6:00</option>
                                <option value="12:00-6:00">12:00 - 6:00</option>
                            @elseif($data->time == 'dinner')
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch" >Lunch</option>
                                <option value="dinner" selected >Dinner</option>
                                <option value="10:00-12:00">10:00 - 12:00</option>
                                <option value="4:00-6:00">4:00 - 6:00</option>
                                <option value="12:00-6:00">12:00 - 6:00</option>
                            @elseif($data->time == '10:00-12:00')
                                <option value="breakfast" >Breakfast</option>
                                <option value="lunch" >Lunch</option>
                                <option value="dinner">Dinner</option>
                                <option value="10:00-12:00" selected>10:00 - 12:00</option>
                                <option value="4:00-6:00">4:00 - 6:00</option>
                                <option value="12:00-6:00">12:00 - 6:00</option>
                             @elseif($data->time == '4:00-6:00')
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch">Lunch</option>
                                <option value="dinner">Dinner</option>
                                <option value="10:00-12:00">10:00 - 12:00</option>
                                <option value="4:00-6:00" selected>4:00 - 6:00</option>
                                <option value="12:00-6:00">12:00 - 6:00</option>
                            @elseif($data->time == '12:00-6:00')
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch" >Lunch</option>
                                <option value="dinner" >Dinner</option>
                                <option value="10:00-12:00">10:00 - 12:00</option>
                                <option value="4:00-6:00">4:00 - 6:00</option>
                                <option value="12:00-6:00" selected>12:00 - 6:00</option>
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="form-group col-md-6" >
                            <label for="">Select Dish</label><br>
                                <select multiple class="chosen-select form-control" name="dish_name[]" >
                                <option value="{{ $data->dish_name }}" selected>{{ $data->dish_name }}</option>
                                     @foreach ($product as $prd)
                                        <option value="{{ $prd['prd_name'] }}">{{ $prd['prd_name'] }}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 " style="margin-top:3%">
                            <button class="btn-success btn col-md" type="submit" name="update_tea" value="update_tea">Update Tea Schedule</button>
                        </div>
                        <div class="col-sm-6" style="margin-top:3%">
                        <button class="btn btn-primary pull-right" type="submit" name="add_tea" value="add_tea">Add Tea Schedule</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
     </div>
     @push('css')

            <style>
                .chosen-container{
                    width: 100% !important;
                }
            </style>
            @endpush
 @stop
@section('footer_scripts')
<script src="js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="{{ asset('js/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.extensions.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('js/icheck.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="{{ asset('js/chosen.jquery.js') }}"></script>

<script>

$(document).ready(function(){
    var $chosen = $('.chosen-select').chosen({
  max_selected_options: 6
});

$chosen.change(function () {
  var $this = $(this);
  var chosen = $this.data('chosen');
  var search = chosen.search_container.find('input[type="text"]');

  search.prop('disabled', $this.val() !== null);

  if (chosen.active_field) {
    search.focus();
  }
});
    $('#meal').click(function() {
        $('.tea').hide();
      $('.meal').show();
    });
    $('#tea').click(function() {
        $('.meal').hide();
      $('.tea').show();
    });

});
</script>
@endsection
