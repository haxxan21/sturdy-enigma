@extends('index')
@section('content')
<div class="row">
    <div class="col-xs-4 ">
        <div class="input-group">
            <input type="text" class="form-control" onkeyup="search_name()" id="name"  placeholder="Jon Doe">
            <div class="input-group-btn search-panel">
                <button class="btn btn-success" style="border:none">
                    <span id="search_concept" style="color:white">Search By Name</span>
                </button>
            </div>
            <span class="input-group-btn">
                <i class="far fa-search"></i>
            </span>
        </div>
    </div>
        <div class="col-xs-4 ">
            <div class="input-group">
                <select id="role" class="form-control" onChange="search_role()">
                    <option value=""> </option>
                    <option value="super_admin">Super Admin</option>
                    <option value="admin">Admin</option>
                    <option value="user">User</option>
                    <option value="cook">Cook</option>
                    </select>
                <div class="input-group-btn search-panel">
                    <button class="btn btn-warning" style="border:none">
                        <span id="search_concept" style="color:white">Search By Role</span>
                    </button>
                </div>
                <span class="input-group-btn">
                    <i class="far fa-search"></i>
                </span>
        </div>
    </div>
        <div class="col-xs-4">
                <div class="input-group">
                    <input type="text" id="mobile" placeholder="search by mobile" class="form-control" onkeyup="search_mobile()">
                    <div class="input-group-btn search-panel">
                        <button class="btn btn-primary" style="border:none">
                            <span id="search_concept" style="color:white">Search By Mobile</span>
                        </button>
                    </div>
                    <span class="input-group-btn">
                        <i class="far fa-search"></i>
                    </span>
        </div>
    </div>

    </div>
{{-- <div class="row">
    <div class="col-sm-12">
        <div class="col-sm-4">
            <input type="text" id="name" placeholder="search by name" class="form-control" onkeyup="search_name()">
        </div>
        <div class="col-sm-4">
            <input type="text" id="mobile" placeholder="search by mobile" class="form-control" onkeyup="search_mobile()">
        </div>
        <div class="col-sm-4">
            <select id="role" class="form-control" onChange="search_role()">
            <option value=""> </option>
            <option value="super_admin">Super Admin</option>
            <option value="admin">Admin</option>
            <option value="user">User</option>
            <option value="cook">Cook</option>
            </select>
        </div>
    </div>
</div> --}}
{{-- <div class="row" style="margin-top:3%">
    <div class="col-sm-6">
    <a href="{{route('employee.create')}}">
    <button class="btn btn-success">+ Add New User</button>
    </a>
    </div>
</div> --}}
<div class="row" style="margin-top:4%">
    <div class="col-sm-12" style="background-color:white">
    <table class="table" id="myTable">
        <thead>
            <th>No.</th>
            <th>User Number</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Role</th>
            <th>Status</th>
            @if (Auth::user()->id == 'super_admin')
            <th>Actions</th>
            @endif
        </thead>
        <tbody>
            @foreach ($data as $info)
            <tr>
                <td class="counterCell"></td>
                <td>{{ $info->emp_num }}</td>
            <td>{{ $info->fname }} {{ $info->lname }}</td>
                <td>{{ $info->email }}</td>
                <td>{{ $info->mobile }}</td>
                <td>{{ $info->role }}</td>
                <td>{{ $info->status  }}</td>
                @if (Auth::user()->role == 'super_admin')
            <td><form action="{{ route('employee.destroy' , $info->id) }}" method="POST" data-confirm="Are you sure you want to delete this user?">
                    @CSRF @method('DELETE') <button type="submit" class="btn-xs btn-danger btn pull-left">Delete</button>
            </form><a href="{{ route('employee.edit',$info->id) }}" style="margin-left:1%"><button class="btn btn-xs btn-primary">Edit</button></a></td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
    {{ $data->links() }}
</div>
</div>
@stop
@section('footer_scripts')
<script>

function search_name() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("name");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function search_mobile() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("mobile");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[4];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function search_role() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("role");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[5];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    $(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
    	e.stopImmediatePropagation();
      e.preventDefault();
		}
});
</script>
@endsection
