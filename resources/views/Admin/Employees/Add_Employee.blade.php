@extends('index')
@section('content')

<div class="box-body" style="background-color: white">
<form class="form" enctype="multipart/form-data" action="{{ route('employee.store') }}" method="POST" id="Add_user_form">
        @CSRF
        <div class="row" >
          <div class="col-md-6">
            <div class="form-group">
              <label for="fname">First Name</label>
              <input type="text" class="form-control{{ $errors->has('fname') ? ' is-invalid' : ''}}" name="fname" placeholder="John" id="fname" value="{{ old('fname') }}">
              @if ($errors->has('fname'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('fname') }}</strong>
                    </span>
              @endif
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label for="lname">Last Name</label>
                <input type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : ''}}"  id="lname" name="lname" placeholder="Doe" value="{{ old('lname') }}">
                @if ($errors->has('lname'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('lname') }}</strong>
                </span>
                 @endif
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="mobile">Mobile</label>
                <input type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : ''}}" id="mobile" name="mobile" placeholder="03001234567" value="{{ old('mobile') }}">
                @if ($errors->has('mobile'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('mobile') }}</strong>
                </span>
                @endif
            </div>
            </div>
        <div class="col-sm-6">
                <label for="role">Role</label>
                <select class="form-control select2" style="width: 100%;" name="role" >
                    <option selected="selected" value=""> </option>
                    @if (Auth::user()->role == 'super_admin')
                    <option value="super_admin">Super Admin</option>
                    @endif
                    <option value="admin">Admin</option>
                    <option value="user">User</option>
                    <option value="cook">Cook</option>
                </select>
            </div>
        </div>
        <div class="row" style="margin-top:2%">
            <div class="col-sm-6">
                <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control{{ $errors->has('status') ? 'is-invalid' : '' }}">
                <option value="active" selected>Active</option>
                <option value="inactive">In-Active</option>
                </select>
            </div>
            </div>
            <div class="col-sm-6">
            <div class="form-group">
                <label for="email">Email</label>
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : ''}}" id="email" name="email" placeholder="Someone@example.com" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                     <label for="password">Password</label>
                     <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : ''}}" id="password" name="password" placeholder="********">
                    </div>
                </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="con_pass">Confirm Password</label>
                <input type="password" class="form-control" name="con_pass" placeholder="********">
              </div>
            </div>
        </div>
        <div class="row" style="margin-top:2%">
          <div class="col-md-12">
            <button class="btn btn-success col-sm-3 pull-right" type="submit">Submit</button>
          </div>
        </div>
        </form>
    </div>
</div>


@push('css')
<!-- bootstrap datepicker -->
{{-- <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}"> --}}
<!-- iCheck for checkboxes and radio inputs -->
{{-- <link rel="stylesheet" href="{{ asset('css/iCheck/all.css') }}"> --}}
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush
@stop

@section('footer_scripts')
<script src="{{asset('js/select2.full.js')}}"></script>
<!-- InputMask -->
<script src="{{ asset('js/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.extensions.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('js/icheck.min.js') }}"></script>

@stop
