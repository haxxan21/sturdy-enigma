@extends('index')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <form action="" method="post">
            @csrf
        <div class="form-inline">
           <div class="col-sm-2">
                <label for="category">Category Name:</label>
           </div>
           <div class="col-sm-3">
                <input type="text" name="ctgry_name" placeholder="enter new category" class="form-control" width="100%" >
            </div>
            <div class="col-sm-3">
                <button type="submit" class="btn btn-success">Add New Category</button>
            </div>
        </div>
    </form>
    </div>
</div>
<div class="row" style="margin-top:3%">
    <div class="col-sm-12">
        <table class="table">
            <thead>
                <th>No.</th>
                <th>Category Name</th>
                <th>Num of Users</th>
                <th>Action</th>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@stop
@section('footer_scripts')
    <script>
    $(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
    	e.stopImmediatePropagation();
      e.preventDefault();
		}
});
    </script>
@endsection
