@extends('index')
@section('content')
<div style="margin-bottom:4em">
<form class="form" id="Update_profile_form" action="{{ route('employee.update' , Auth::user()->id) }}" method="post" enctype="multipart/form-data">
        @CSRF
        @method('PATCH')
        <div class="form-group" style="margin-top:2%">
            <input type="file" name="img" class="form-control{{ $errors->has('img') ? ' is-invalid' : ''}}" id="imgInp" >
            @if($errors->has('img'))
                <span class="invalid-feedback">
                    <strong> {{ $errors->first('img')}}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="password">New Password</label>
            <input type="password" name="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : ''}}" placeholder="Enter New Password">
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group" style="margin-bottom:4%">
            <label for="con_pass">Confirm Password</label>
            <input type="password" name="con_pass" id="con_pass" class="form-control{{ $errors->has('conpass') ? ' is-invalid' : ''}}" placeholder="Confirm Password">
            @if($errors->has('con_pass'))
                <span class="invalid-feedback">
                    <strong> {{$errors->first('con_pass')}}</strong>
                </span>
            @endif
        </div>
        <button type="submit" name="update_profile" value="update_profile" class="btn btn-primary form-control">Update Profile</button>
    </form>
</div>
<script>
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function () {
        readURL(this);
    });

</script>
@stop
@section('section')
<h1> Update Profile </h1>
<hr>
<ol class="breadcrumb">
       <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Update Profile</li>
       </ol>
@endsection
