@extends('index')
@section('content')
<div class="box-body" style="background-color:white">
        <form class="form" enctype="multipart/form-data" action="{{ route('employee.update', $data->id) }}" method="POST" id="Update_user_form">
                @CSRF @method('PATCH')
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="fname">First Name</label>
                    <input type="text" class="form-control{{ $errors->has('fname') ? 'is-invalid' : '' }}" name="fname" value="{{ $data->fname }}">
                    @if ($errors->has('fname'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('fname') }}</strong>
                        </span>
                    @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lname">Last Name</label>
                    <input type="text" class="form-control{{ $errors->has('lname') ? 'is-invalid' : '' }}" name="lname" id="lname" value="{{ $data->lname }}">
                    @if ($errors->has('lname'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('lname') }}</strong>
                        </span>
                    @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="mobile">Mobile</label>
                      <input type="text" class="form-control{{ $errors->has('mobile') ? 'is-invalid' : '' }}" id="mobile" name="mobile" value="{{ $data->mobile }}">
                      @if ($errors->has('mobile'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mobile') }}</strong>
                          </span>
                      @endif
                        </div>
                    </div>
                <div class="col-sm-6">
                    <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control{{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ $data->email }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                                <label for="role">Role</label>
                                <select class="form-control select2" style="width: 100%;" name="role">
                                @if($data->role == 'super_admin')
                                    <option value="super_admin" selected>Super Admin</option>
                                    <option value="admin">Admin</option>
                                    <option value="user">User</option>
                                    <option value="cook">Cook</option>
                                @elseif($data->role == 'admin')
                                    <option value="super_admin">Super Admin</option>
                                    <option value="admin" selected>Admin</option>
                                    <option value="user">User</option>
                                    <option value="cook">Cook</option>
                                @elseif($data->role == 'user')
                                    <option value="super_admin">Super Admin</option>
                                    <option value="admin">Admin</option>
                                    <option value="user" selected >User</option>
                                    <option value="cook">Cook</option>
                                @elseif($data->role == 'cook')
                                    <option value="super_admin">Super Admin</option>
                                    <option value="admin">Admin</option>
                                    <option value="user">User</option>
                                    <option value="cook" selected>Cook</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            @if ($data->status == 'active')
                                <option value="active" selected>Active</option>
                                <option value="inactive">In-Active</option>
                            @elseif($data->status == 'inactive')
                                <option value="active">Active</option>
                                <option value="inactive" selected>In-Active</option>
                            @elseif($data->status == null)
                                <option value="active" selected>Active</option>
                                <option value="inactive">In-Active</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-top:4%">
                  <div class="col-md-12">
                    <button class="btn btn-success col-sm-3 pull-left" type="submit">Submit</button>
                  </div>
                </div>
                </form>
            </div>
@stop
@section('section')
<h1>Edit Employee</h1>
<hr>
<ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Employee</li>
        </ol>
@endsection
