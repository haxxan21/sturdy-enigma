@extends('index')
@section('content')
    <div class="row">
        <div class="col-sm-12" style="background-color:white">
            <form action="{{ route('dish.update' , $data->id) }}" class="form" method="POST" enctype="multipart/form-data" id="Update_dish_form">
                @method('PUT')
            {{ csrf_field() }}
            <div class="text-center">
            <img src="{{ asset('dish_img/'.$data->img) }}" alt="{{ $data->dish_name }}" style="border-radius:50%">
            </div>
            <div class="row" style="margin-top:3%">
            <div class="form-group col-sm-6">
                <label for="dish_name">Dish Name</label>
            <input type="text" class="form-control{{ $errors->has('dish_name') ? 'is-invalid' : '' }}" placeholder="Enter Dish Name" id="dish_name" name="dish_name" value="{{ $data->dish_name }}">
            @if ($errors->has('dish_name'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('dish_name') }}</strong>
                </span>
            @endif
            </div>
            <div class="col-sm-6 form-group">
                <label for="dish_price">Dish Price</label>
            <input type="text" class="form-control{{ $errors->has('dish_price') ? 'is-invalid' : '' }}" name="dish_price" id="dish_price" value="{{ $data->dish_price }}">
            @if ($errors->has('dish_price'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('dish_price') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <label for="img">Upload Image</label>
                <input type="file" class="form-control{{ $errors->has('img') ? 'is-invalid' : '' }}" name="img" id="img">
                @if ($errors->has('img'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('img') }}</strong>
                    </span>
                @endif
                </div>
            </div>
            <div class="row" style="margin-top:4%">
                <div class="col-sm-11">
                    <button class="btn-primary btn pull-right" type="submit" name="submit">Submit</button>
                </div>
            </div>
        </form>
        </div>
    </div>
    @stop
    @section('footer_scripts')
    <script>
            $(document).ready(function(){
$('input#dish_price').blur(function(){
    var num = parseFloat($(this).val());
    var cleanNum = num.toFixed(2);
    $(this).val(cleanNum);
    if(num/cleanNum < 1){
        $('#error').text('Please enter only 2 decimal places, we have truncated extra points');
        }
    });
});
    </script>
    @endsection
