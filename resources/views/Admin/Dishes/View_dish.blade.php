@extends('index')
@section('content')

    <div class="row">
    <div class="col-xs-6 col-xs" style="margin-top:4%;">
        <div class="input-group">
            <input type="hidden" name="search_param" value="all" id="search_param">
            <input type="text" class="form-control" onkeyup="myFunction()" id="myInput"  placeholder="Jon Doe">
            <div class="input-group-btn search-panel">
                <button class="btn btn-success">
                    <span id="search_concept" style="color:white">Search By Name</span>
                </button>
            </div>
            <span class="input-group-btn">
                <i class="far fa-search"></i>
            </span>
        </div>
    </div>
    </div>
    <div class="row" style="margin-top:4%;">
        <div class="col-sm-12" style="background-color:white">
            <table class="table" id="myTable">
                <thead>
                    <th>Num</th>
                    <th>Dish Name</th>
                    <th>Dish Price</th>
                    <th>Date Added</th>
                    <th>Dish Image</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($data as $info)
                    <tr>
                    <td class="counterCell"></td>
                    <td>{{ $info['dish_name'] }}</td>
                    <td>{{ $info['dish_price'] }}</td>
                    <td>{{ substr($info->created_at , 0, strrpos($info->created_at , ' ')) }}</td>
                    <td><img src="{{ asset('/dish_img/'.$info['img']) }}" alt="{{ $info->dish_name }}" height="50px" width="50px" class="modal-img" id="imageresource"></td>
                    <td>@if($info->id != 1 && $info->id != 5)
                        <form action="{{ route('dish.destroy', $info->id) }}" method="post" class="pull-left" data-confirm="Are you sure you want to delete this product?">
                            {{ method_field('DELETE') }}
                            @CSRF
                            <button class="btn btn-danger btn-xs" type="submit">Delete</button>
                        </form> @else <button class="btn btn-danger btn-xs" onclick="prevent()">Delete</button> @endif
                    <a href="{{ route('dish.edit', $info->id) }}"><button class="btn-primary btn btn-xs" style="margin-left:12%">Edit</button></a>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $data->links() }}
        </div>
    </div>
    @push('css')
    <style>
    img.modal-img {
        cursor: pointer;
        transition: 0.3s;
    }

    img.modal-img:hover {
        opacity: 0.7;
    }

    .img-modal {
        display: none;
        position: fixed;
        z-index: 99999;
        padding-top: 100px;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgba(0, 0, 0, 0.9);
    }

    .img-modal img {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 200%;
    }

    .img-modal div {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 200px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 100px;
    }

    .img-modal img,
    .img-modal div {
        animation: zoom 0.6s;
    }

    .img-modal span {
        position: absolute;
        top: 15px;
        right: 35px;
        color: #f1f1f1;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
        cursor: pointer;
    }

    @media only screen and (max-width: 200px) {
        .img-modal img {
            width: 100%;
        }
    }

    @keyframes zoom {
        0% {
            transform: scale(0);
        }

        100% {
            transform: scale(1);
        }
    }

    .modal-full {
        min-width: 80%;
        margin-left: 19%;
    }

    .modal-full .modal-content {
        min-height: 100vh;
    }

    .button_box2 {
margin:100px auto;
}
/*-------------------------------------*/
.cf:before, .cf:after{
content:"";
display:table;
}
.cf:after{
clear:both;
}
.cf{
zoom:1;
}
/*-------------------------------------*/

.form-wrapper-2 {
width: 330px;
padding: 15px;
background: #555;
-moz-border-radius: 10px;
-webkit-border-radius: 10px;
border-radius: 10px;
-moz-box-shadow: 0 1px 1px rgba(0,0,0,.4) inset, 0 1px 0 rgba(255,255,255,.2);
-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.4) inset, 0 1px 0 rgba(255,255,255,.2);
box-shadow: 0 1px 1px rgba(0,0,0,.4) inset, 0 1px 0 rgba(255,255,255,.2);
}
.form-wrapper-2 input {
width: 210px;
height: 20px;
padding: 10px 5px;
float: left;
font: bold 15px 'Raleway', sans-serif;
border: 0;
background: #eee;
-moz-border-radius: 3px 0 0 3px;
-webkit-border-radius: 3px 0 0 3px;
border-radius: 3px 0 0 3px;
}
.form-wrapper-2 input:focus {
outline: 0;
background: #fff;
-moz-box-shadow: 0 0 2px rgba(0,0,0,.8) inset;
-webkit-box-shadow: 0 0 2px rgba(0,0,0,.8) inset;
box-shadow: 0 0 2px rgba(0,0,0,.8) inset;
}
.form-wrapper-2 input::-webkit-input-placeholder {
color: #999;
font-weight: normal;
font-style: italic;
}
.form-wrapper-2 input:-moz-placeholder {
color: #999;
font-weight: normal;
font-style: italic;
}
.form-wrapper-2 input:-ms-input-placeholder {
color: #999;
font-weight: normal;
font-style: italic;
}
.form-wrapper-2 button {
overflow: visible;
position: relative;
float: right;
border: 0;
padding: 0;
cursor: pointer;
height: 40px;
width: 110px;
font: bold 15px/40px 'Raleway', sans-serif;
color: #fff;
text-transform: uppercase;
background: #D88F3C;
-moz-border-radius: 0 3px 3px 0;
-webkit-border-radius: 0 3px 3px 0;
border-radius: 0 3px 3px 0;
text-shadow: 0 -1px 0 rgba(0, 0 ,0, .3);
}
.form-wrapper-2 button:hover{
background: #FA8807;
}
.form-wrapper-2 button:active,
.form-wrapper-2 button:focus{
background: #c42f2f;
}
.form-wrapper-2 button:before {
content: '';
position: absolute;
border-width: 8px 8px 8px 0;
border-style: solid solid solid none;
border-color: transparent #D88F3C transparent;
top: 12px;
left: -6px;
}
.form-wrapper-2 button:hover:before{
border-right-color: #FA8807;
}
.form-wrapper-2 button:focus:before{
border-right-color: #c42f2f;
}
.form-wrapper-2 button::-moz-focus-inner {
border: 0;
padding: 0;
}
    </style>
    @endpush
@stop
@section('footer_scripts')
    <script>
            $('img.modal-img').each(function () {
            var modal = $('<div class="img-modal"><span>&times;</span><img /><div></div></div>');
            modal.find('img').attr('src', $(this).attr('src'));
            if ($(this).attr('alt'))
                modal.find('div').text($(this).attr('alt'));
            $(this).after(modal);
            modal = $(this).next();
            $(this).click(function (event) {
                modal.show(300);
                modal.find('span').show(0.3);
            });
            modal.find('span').click(function (event) {
                modal.hide(300);
            });
        });
        $(document).keyup(function (event) {
            if (event.which == 27)
                $('.img-modal>span').click();
        });

        function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function prevent() {
  alert("Cannot Delete This Dish!");
}
$(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
    	e.stopImmediatePropagation();
      e.preventDefault();
		}
});
    </script>
@endsection
