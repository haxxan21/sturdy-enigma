@extends('index')
@section('content')
<div class="row">
    <div class="col-xs-4 ">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search by Name" onkeyup="s_name()" id="myInputname">
            <div class="input-group-btn search-panel">
                <button class="btn btn-success" style="border:none">
                    <span id="search_concept" style="color:white">Search By Name</span>
                </button>
            </div>
            <span class="input-group-btn">
                <i class="far fa-search"></i>
            </span>
        </div>
    </div>
        <div class="col-xs-4 ">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search by Category" onkeyup="category()" id="Input1">
                <div class="input-group-btn search-panel">
                    <button class="btn btn-warning" style="border:none">
                        <span id="search_concept" style="color:white">Search By Time</span>
                    </button>
                </div>
                <span class="input-group-btn">
                    <i class="far fa-search"></i>
                </span>
        </div>
    </div>
        <div class="col-xs-4">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search by Price" onkeyup="price()" id="Input2">
                    <div class="input-group-btn search-panel">
                        <button class="btn btn-primary" style="border:none">
                            <span id="search_concept" style="color:white">Search By price</span>
                        </button>
                    </div>
                    <span class="input-group-btn">
                        <i class="far fa-search"></i>
                    </span>
        </div>
    </div>

    </div>
    <div class="row" style="margin-top:4%">
        <div class="col-sm-12" style="background-color:white">
        <table class="table" id="myTable">
            <thead>
                <th>No.</th>
                <th>Name</th>
                <th>Category</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Date Added</th>
                <th>Product Image</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach($data as $info)
                <tr>
                    <td class="counterCell"></td>
                <td>{{ $info->prd_name }}</td>
                <td>{{ $info->prd_ctgry }}</td>
                <td>{{ $info->prd_price }}</td>
                <td>{{ $info->prd_qty }}</td>
                <td>{{ substr($info->created_at , 0, strrpos($info->created_at , ' ')) }}</td>
                <td><img src="{{ asset('/prd_img/'.$info->img) }}" alt="{{ $info->prd_name }}" width="50px" height="50px" class="modal-img" id="imageresource"></td>
                <td><form action="{{ route('product.destroy', $info->id) }}" method="post" data-confirm="Are you sure you want to delete this product?">
                    @method('DELETE')
                    @CSRF
                    <button class="btn btn-danger btn-xs pull-left" type="submit" >Delete</button>
                </form><a href="{{ route('product.edit', $info->id) }}" style="margin-left:3%"><button class="btn btn-primary btn-xs pull-left">Edit</button></a>
            <a href="{{ route('inventory.show',$info->id) }}"><button class="btn btn-default btn-xs pull-left">Inventory</button></a>
            </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $data->links() }}
    </div>
    </div>
    @push('css')
    <style>
    img.modal-img {
        cursor: pointer;
        transition: 0.3s;
    }

    img.modal-img:hover {
        opacity: 0.7;
    }

    .img-modal {
        display: none;
        position: fixed;
        z-index: 99999;
        padding-top: 100px;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgba(0, 0, 0, 0.9);
    }

    .img-modal img {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 200%;
    }

    .img-modal div {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 200px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 100px;
    }

    .img-modal img,
    .img-modal div {
        animation: zoom 0.6s;
    }

    .img-modal span {
        position: absolute;
        top: 15px;
        right: 35px;
        color: #f1f1f1;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
        cursor: pointer;
    }

    @media only screen and (max-width: 200px) {
        .img-modal img {
            width: 100%;
        }
    }

    @keyframes zoom {
        0% {
            transform: scale(0);
        }

        100% {
            transform: scale(1);
        }
    }

    .modal-full {
        min-width: 80%;
        margin-left: 19%;
    }

    .modal-full .modal-content {
        min-height: 100vh;
    }
    </style>
    @endpush
    @stop
    @section('footer_scripts')
        <script>
                $('img.modal-img').each(function () {
                var modal = $('<div class="img-modal"><span>&times;</span><img /><div></div></div>');
                modal.find('img').attr('src', $(this).attr('src'));
                if ($(this).attr('alt'))
                    modal.find('div').text($(this).attr('alt'));
                $(this).after(modal);
                modal = $(this).next();
                $(this).click(function (event) {
                    modal.show(300);
                    modal.find('span').show(0.3);
                });
                modal.find('span').click(function (event) {
                    modal.hide(300);
                });
            });
            $(document).keyup(function (event) {
                if (event.which == 27)
                    $('.img-modal>span').click();
            });

            function s_name() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInputname");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function category() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("Input1");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function price() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("Input2");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[3];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

$(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
    	e.stopImmediatePropagation();
      e.preventDefault();
		}
});

        </script>
    @endsection
