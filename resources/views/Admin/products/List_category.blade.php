@extends('index')
@section('content')
<div class="row">
    <div class="col-sm-12">
    <form action="{{ route('productcat.store') }}" method="post" id="Add_category_form">
            @csrf
        <div class="form-inline">
           <div class="col-sm-2">
                <label for="category">Category Name:</label>
           </div>
           <div class="col-sm-3">
           <input type="text" name="category_name" placeholder="enter new category" class="form-control{{ $errors->has('category_name') ? 'is-invalid' : '' }}" width="100%" value="{{ old('category_name') }}">
           @if ($errors->has('category_name'))
               <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->has('category_name') }}</strong>
               </span>
           @endif
            </div>
            <div class="col-sm-3">
                <button type="submit" class="btn btn-success">Add New Category</button>
            </div>
        </div>
    </form>
    </div>
</div>
<hr>
{{-- <div class="row">
        <div class="col-sm-12">
            <p style="padding:1%"><strong>Total Number of Categories</strong></p><hr>
            <table class="table table-bordered table-condensed">
                 <thead>
                    <th>No.</th>
                    <th>Category Name</th>
                    <th>Date Added</th>
                </thead>
                <tbody>
                    @foreach ($category as $info)
                    <tr>
                     <td class="counterCell"></td>
                     <td>{{ $info->category_name }}</td>
                     <td>{{ substr($info->created_at , 0, strrpos($info->created_at , ' ')) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div> --}}
<div class="row" style="margin-top:3%">
    <div class="col-sm-12" style="background-color:white">
        <p style="padding:1%"><strong>Number of Products Added in Categories</strong></p><hr>
        <table class="table">
            <thead>
                <th>No.</th>
                <th>Category Name</th>
                <th>Num of Products</th>
                <th>Date</th>
                <th>Action</th>
            </thead>
            <tbody >
                @foreach ($data as $info)
                <tr>
                <td class="counterCell" ></td>
                <td>{{ $info['category'] }}</td>
                <td>{{ $info['products'] }} </td>
                <td>{{ substr($info['date'] , 0, strrpos($info['date'] , ' ')) }}</td>
                <td><form action="{{ route('productcat.destroy', $info['category']) }}" method="post" data-confirm="Are you sure you want to delete this category?">
                    @method('DELETE')
                    @CSRF
                    <button class="btn btn-danger btn-xs pull-left" type="submit">Delete</button>
                </form><button class="btn-primary btn btn-xs pull-left" value="{{ $info['category'] }}" id="edit">Edit</button></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Edit Category</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                <form action="{{ route('productcat.update', $info['category']  ) }}" method="post">
                    @method('PUT')
                    @CSRF
                    <div class="form-group">
                        <label for="category_name">Category Name</label>
                        <div class="form-group">
                        <input type="hidden" value="" id="getid" name="id" class="in">
                            <input type="text" name="category_name" value="" class="form-control in" id="ctgry">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit" id="btn">Update</button>
                        </div>
                    </div>
                </form>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
@stop
@section('footer_scripts')
    <script>
    $(document).on('click', '#edit', function () {
            var url = "/productcat";
            var url2 = "/edit"
            var id = $(this).val();
            $.get(url + '/' + id + url2, function (data) {
               ctgry_name = data.ctgry[0].category_name
               id = data.ctgry[0].id
               var input = $("#ctgry")
               var input2 = $('#getid')
                    input.val(input.val() + ctgry_name)
                    input2.val(input2.val() + id)
               $("#myModal").modal("show")
            })
        });
    $('#MyModal').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
})
$(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
    	e.stopImmediatePropagation();
      e.preventDefault();
		}
});
    </script>
@endsection
