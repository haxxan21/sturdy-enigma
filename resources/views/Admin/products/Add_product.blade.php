@extends('index')
@section('content')

<div class="row">
    <div class="col-sm-12" style="background-color:white">
    <form action="{{ route('product.store') }}" class="form" method="POST" enctype="multipart/form-data" id="Add_product_form">
        @CSRF
        <div class="row" style="margin-top:3%">
            <div class="form-group col-md-6">
                <label for="prd_name">Product Name</label>
            <input type="text" class="form-control{{ $errors->has('prd_name') ? 'is-invalid' : '' }}" name="prd_name" id="prd_name" placeholder="Enter Product Name" value="{{ old('prd_name') }}">
            @if ($errors->has('prd_name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('prd_name') }}</strong>
                </span>
            @endif
            </div>
            <div class="form-group col-md-6">
                <label for="prd_ctgry">Product Category</label>
            <select name="prd_ctgry" id="prd_ctgry" class="form-control{{ $errors->has('prd_ctgry') ? 'is-invalid' : '' }}" value="{{ old('prd_ctgry') }}">
                    <option value=""></option>
                    @foreach($data as $info)
                <option value="{{ $info['category_name'] }}">{{ $info['category_name'] }}</option>
                    @endforeach
                </select>
                @if ($errors->has('prd_ctgry'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('prd_ctgry') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Price</label>
            <input type="text" class="form-control{{ $errors->has('prd_price') ? 'is-invalid' : '' }}" name="prd_price" id="prd_price" value="{{ old('prd_price') }}">
            @if ($errors->has('prd_price'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('prd_price') }}</strong>
                </span>
            @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="prd_qty">Product Quantity</label>
            <input type="number" class="form-control{{ $errors->has('prd_qty') ? 'is-invalid' : ''}}" id="prd_qty" name="prd_qty">
            @if ($errors->has('prd_qty'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('prd_qty') }}</strong>
            </span>
            @endif
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="img">Upload Image</label>
                    <input type="file" class="form-control{{ $errors->has('img') ? 'is-invalid' : '' }}" name="img" id="img">
                    @if ($errors->has('img'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('img') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:2%">
            <div class="col-md-12">
                <button class="btn btn-success pull-right" type="submit" name="submit" value="submit">Submit</button>
            </div>
        </div>
        </form>
    </div>
    </div>

    @stop
    @section('footer_scripts')
        <script>
                $(document).ready(function(){
    $('input#prd_price').blur(function(){
        var num = parseFloat($(this).val());
        var cleanNum = num.toFixed(2);
        $(this).val(cleanNum);
        if(num/cleanNum < 1){
            $('#error').text('Please enter only 2 decimal places, we have truncated extra points');
            }
        });
    });
        </script>
    @endsection
