@extends('index')
@section('content')

<div class="row">
    <div class="col-sm-12" style="background-color:white">
        <div class="text-center">
        <img src="{{ asset('/prd_img/'.$data['img']) }}" alt="{{ $data['prd_name'] }}" style="border-radius:50%" width="30%" height="30%">
        </div>
    <form action="{{ route('product.update', $data->id) }}" class="form" method="POST" enctype="multipart/form-data" id="Update_product_form">
       @method('PUT')
        @CSRF
        <div class="row" style="margin-top:4%">
            <div class="form-group col-md-6">
                <label for="prd_name">Product Name</label>
            <input type="text" class="form-control{{ $errors->has('prd_name') ? 'is-nvalid' : '' }}" name="prd_name" value="{{ $data['prd_name'] }}">
            @if ($errors->has('prd_name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('prd_name') }}</strong>
                </span>
            @endif
            </div>
            <div class="form-group col-md-6">
                <label for="prd_ctgry">Product Category</label>
            <select name="prd_ctgry" id="prd_ctgry" class="form-control{{ $errors->has('prd_ctgry') ? 'is-invalid' : '' }}">
                <option value="{{ $data['prd_ctgry'] }}" selected>{{ $data['prd_ctgry'] }}</option>
                    @foreach($category as $info)
                <option value="{{ $info['category_name'] }}">{{ $info['category_name'] }}</option>
                    @endforeach
                </select>
                @if ($errors->has('prd_ctgry'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('prd_ctgry') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Price</label>
            <input type="text" class="form-control{{ $errors->has('prd_price') ? 'is-invalid' : '' }}" id="prd_price" name="prd_price" value="{{ $data['prd_price'] }}">
            @if ($errors->has('prd_price'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('prd_price') }}</strong>
                </span>
            @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="prd_qty">Product Quantity</label>
            <input type="number" class="form-control{{ $errors->has('prd_qty') ? 'is-invalid' : ''}}" name="prd_qty" id="prd_qty" value="{{ $data['prd_qty'] }}">
            @if ($errors->has('prd_qty'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('prd_qty') }}</strong>
                </span>
            @endif
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="gorm-group">
                    <label for="img">Upload Image</label>
                    <input type="file" class="form-control{{ $errors->has('img') ? 'is-invalid' : '' }}" id="img" name="img">
                    @if ($errors->has('img'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('img') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:2%">
            <div class="col-md-12">
                <button class="btn btn-success pull-right" type="submit" name="submit" value="submit">Submit</button>
            </div>
        </div>
        </form>
    </div>
    </div>
    @stop
    @section('footer_scripts')
        <script>
        $(document).ready(function(){
    $('input#prd_price').blur(function(){
        var num = parseFloat($(this).val());
        var cleanNum = num.toFixed(2);
        $(this).val(cleanNum);
        if(num/cleanNum < 1){
            $('#error').text('Please enter only 2 decimal places, we have truncated extra points');
            }
        });
    });
        </script>
    @endsection
