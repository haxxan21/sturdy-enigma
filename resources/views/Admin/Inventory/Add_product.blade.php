@extends('index')
@section('content')

<div class="row">
    <div class="col-sm-12" style="background-color:white">
        <form action="" class="form" method="POST" enctype="multipart/form-data">
        @CSRF
        <div class="row">
            <div class="form-group col-md-6">
                <label for="prod_name">Product Name</label>
                <input type="text" class="form-control" name="prod_name" placeholder="Enter Product Name">
            </div>
        </div>
        <div class="row" style="margin-top:2%">
            <div class="col-md-6">
                <button class="btn btn-primary" type="submit" name="submit" value="submit">Submit</button>
            </div>
        </div>
        </form>
    </div>
    </div>

@endsection
