@extends('index')
@section('content')
<div class="row">
    <div class="col-xs-6 ">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search by Name" onkeyup="search_name()" id="s_name">
            <div class="input-group-btn search-panel">
                <button class="btn btn-success" style="border:none">
                    <span id="search_concept" style="color:white">Search By Name</span>
                </button>
            </div>
            <span class="input-group-btn">
                <i class="far fa-search"></i>
            </span>
        </div>
    </div>
        <div class="col-xs-6 ">
            <div class="input-group">
                <input type="text" id="s_name" class="form-control" placeholder="search by product name" onkeyup="search_name()">
                <div class="input-group-btn search-panel">
                    <button class="btn btn-warning" style="border:none">
                        <span id="search_concept" style="color:white">Search By category</span>
                    </button>
                </div>
                <span class="input-group-btn">
                    <i class="far fa-search"></i>
                </span>
        </div>
    </div>
    </div>
    <hr>
    <div class="row" style="margin-top:4%">
        <div class="col-sm-12">
        <a href="{{ route('inventory.pdf') }}"><button class="btn btn-primary btn-xs pull-right" style="margin-left:4%">Export to PDF</button></a>
        <a href="{{ route('inventory.excel') }}"><button class="btn-success btn btn-xs pull-right">Export to Excel</button></a>
        </div>
    </div>
    <hr>
    <div class="row" style="background-color:white; margin-top:4%">
        <table class="table" id="myTable">
            <thead>
                <th>No.</th>
                <th>Product Name</th>
                <th>Product Category</th>
                <th>Quantity</th>
                <th>Date Added</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach($data as $info)
                <tr>
                    <td class="counterCell"></td>
                <td>{{ $info->products }}</td>
                <td>{{ $info->category }}</td>
                <td>{{ $info->quantity }}</td>
                <td>{{ $info->date }}</td>
                <td><form action="{{ route('inventory.destroy',$info->id) }}" method="POST"  data-confirm="Are you sure you want to delete this row?">
                @CSRF @method('DELETE')<button type="submit" class="btn-danger btn btn-xs pull-left">Delete</button>
                </form><a href="{{ route('inventory.edit',$info->id) }}"><button class="btn btn-primary btn-xs pull-left">Edit</button></a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $data->links() }}
    </div>
@stop
@section('footer_scripts')
<script>
    function search_name() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("s_name");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function search_cat() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("c_name");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    $(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
    	e.stopImmediatePropagation();
      e.preventDefault();
		}
});
</script>
@endsection
