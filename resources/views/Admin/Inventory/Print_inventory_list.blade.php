
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


<style>
        table {
                   counter-reset: tableCount;
               }

               .counterCell:before {
                   content: counter(tableCount);
                   counter-increment: tableCount;
               }
       </style>
</head>
<body>
        <table class="table" style="width:100%">
                <tr>
                    <th>No.</th>
                    <th>Product Name</th>
                    <th>Product Category</th>
                    <th>Quantity</th>
                    <th>Date Added</th>
                </tr>
                <tbody>
                    @foreach ($datas as $info)
                    <tr>
                    <td class="counterCell"></td>
                    <td>{{ $info->products }}</td>
                    <td>{{ $info->category }}</td>
                    <td>{{ $info->quantity }}</td>
                    <td>{{ $info->date }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <?php  //dd($datas); ?>
        </table>

</body>

</html>
