@extends('index')
@section('content')
<div class="row" style="background-color:white">
    <div class="col-sm-12" >
        <table class="table">
            <tbody>
                <tr>
                    <td><strong>Product Name</strong></td>
                <td>{{ $data->prd_name }}</td>
                </tr>
                <tr>
                    <td><strong>Product Category</strong></td>
                <td>{{ $data->prd_ctgry }}</td>
                </tr>
                <tr>
                    <td><strong>Product Price</strong></td>
                <td>{{ $data->prd_price }}</td>
                </tr>
                <tr>
                    <td><strong>Available Quantity</strong></td>
                    <td>
                    @if($data->prd_qty >= 10 && $data->prd_qty <= 20 )
                    <strong style="background-color:yellow; padding:2%">{{ $data->prd_qty }}</strong>
                    @elseif($data->prd_qty > 20)
                    <strong style="background-color:green; padding:2%">{{ $data->prd_qty }}</strong>
                    @elseif($data->prd_qty < 10)
                    <strong style="background-color:red; color:white; padding:2%">{{ $data->prd_qty }}</strong>
                     @endif
                    </td>
                </tr>
            </tbody>

        </table>
    </div>
    <hr><br>
    <div class="col-sm-12">
    <form action="{{ route('inventory.store') }}" class="form" method="POST" enctype="multipart/form-data" id="Add_inventory_form" data-confirm="Are you sure you want to add this quantity?">
        @CSRF
        <div class="row">
        <input type="hidden" name="prd_id" value="{{ $data->id }}" readonly>
        <div class="form-group col-md-6">
                <label for="">Product Quantity</label>
        <input type="number" name="quantity" class="form-control{{ $errors->has('quantity') ? 'is-invalid' : ''}}" id="quantity" value="{{ old('quantity') }}">
        @if ($errors->has('quantity'))
            <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('quantity') }}</strong>
            </span>
        @endif
        </div>
        <div class="col-md-6">
                <label for="">Date</label>
        <input type="date" class="form-control{{ $errors->has('date') ? 'is-invalid' : ''}}" name="date" id="date" value="{{ old('date') }}">
        @if ($errors->has('date'))
             <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
        @endif
        </div>
        </div>
        <div class="row" style="margin-top:2%">
            <div class="col-md-12">
                <button class="btn btn-success pull-right" type="submit" name="submit" value="submit">Submit</button>
            </div>
        </div>
        </form>
    </div>
    </div>

@stop
@section('section')
<h1>Transfer Product</h1>
<hr>
<ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transfer Product</li>
        </ol>
@stop
@section('footer_scripts')
<script>
$(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
    	e.stopImmediatePropagation();
      e.preventDefault();
		}
});
</script>
@endsection
