@extends('index')
@section('content')

<div class="col-sm-12" style="background-color:white">
        <form action="{{ route('inventory.update', $data->id) }}" class="form" method="POST" enctype="multipart/form-data">
            @CSRF
            @method('PATCH')
            <div class="row">
            <div class="form-group col-md-6">
                    <label for="">Product Quantity</label>
            <input type="number" name="quantity" class="form-control{{ $errors->has('quantity') ? 'is-invalid' : '' }}" value="{{ $data->quantity }}">
            @if ($errors->has('quantity'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('quantity') }}</strong>
                </span>
            @endif
            </div>
            <div class="col-md-6">
                    <label for="">Date</label>
            <input type="date" class="form-control{{ $errors->has('date') ? 'is-invalid' : '' }}" name="date" value="{{ $data->date }}">
            @if ($errors->has('date'))
                <strong>{{ $errors->first('date') }}</strong>
            @endif
            </div>
            </div>
            <div class="row" style="margin-top:2%">
                <div class="col-md-12">
                    <button class="btn btn-success pull-right" type="submit" name="submit" value="submit">Submit</button>
                </div>
            </div>
            </form>
        </div>
        </div>

@stop
@section('section')
<h1>Edit Inventory</h1>
<hr>
<ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Inventory</li>
        </ol>
@endsection
