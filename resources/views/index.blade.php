<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Canteen Management System </title>

  <!-- Bootstrap core CSS -->

  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

  <link href="{{asset('fonts/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="{{asset('css/custom.css')}}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('css/maps/jquery-jvectormap-2.0.3.css')}}" />
  <link href="{{asset('css/icheck/flat/green.css')}}" rel="stylesheet" />
  <link href="{{asset('css/floatexamples.css')}}" rel="stylesheet" type="text/css" />

  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/nprogress.js')}}"></script>
  <link rel="stylesheet" href=" {{asset('css/chosen.css') }}">

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        @stack('css')
<style>
table {
            counter-reset: tableCount;
        }

        .counterCell:before {
            content: counter(tableCount);
            counter-increment: tableCount;
        }

#logout:hover{
    background-color: #ffffff;
}
</style>
</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="{{route('admin.dashboard')}}" class="site_title"><i class="fa fa-spoon ga-lg" ></i> <span style="font-size:18px">Canteen Management System</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="{{asset('/user_img/'.Auth::user()->img)}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
            <h2>{{Auth::user()->fname}} {{ Auth::user()->lname }}</h2>
            </div>
          </div>
          <div class="clear-fix"></div>
          <!-- /menu prile quick info -->

          <br />
          <br><br><br><br>
                    <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <ul class="nav side-menu"  style="margin-top:4%">
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i> Home</span></a>
                </li>
                @if( Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin')
                <li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{ route('employee.create') }}">Add Users</a>
                    </li>
                    <li><a href="{{ route('employee.index') }}">View Users</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-spoon"></i> Dishes <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{ route('dish.create') }}">Add New Dish</a>
                    </li>
                    <li><a href="{{ route('dish.index') }}">View Dish</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-table"></i> Meal Schedule <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{ route('meal.create') }}">Schedule Meal/Tea</a>
                    </li>
                    <li><a href="{{ route('meal.index') }}">View Meal Schedule</a>
                    </li>
                    <li><a href="{{ route('tea.index') }}">View Tea Schedule</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-gift"></i> Products <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none">
                      <li><a href="{{ route('product.create') }}">Add Product</a>
                      </li>
                      <li><a href="{{ route('product.index') }}">View Products</a>
                      </li>
                      <li><a href="{{ route('productcat.index') }}">List Categories</a>
                      </li>
                    </ul>
                  </li>
                <li><a><i class="fa fa-money"></i> Inventory <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display:none">
                    <li><a href="{{ route('inventory.index') }}">View Inventory</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-bar-chart-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="#">View Reports</a>
                    </li>
                    <li><a href="#">Employee Reports (daily)</a>
                    </li>
                    <li><a href="#">Food Expense (monthly)</a>
                    </li>
                    <li><a href="#">Orders Placed (monthly)</a>
                    </li>
                    <li><a href="#">Meal Consumption</a>
                    </li>
                    <li><a href="#">Sales Report (daily)</a>
                    </li>
                    <li><a href="#">Stock/Inventory Reports</a>
                    </li>
                    <li><a href="#">System handlers</a>
                    </li>
                  </ul>
                </li>
                @elseif(Auth::user()->role == 'user')
                <li><a><i class="fa fa-bar-chart-o"></i> Take Order <span class="fa fa-chevron-down"></span></a>
                  </li>
                <li><a><i class="fa fa-bar-chart-o"></i> Print Orders <span class="fa fa-chevron-down"></span></a>
                  </li>
                @elseif(Auth::user()->role == 'cook')
                  <li><a><i class="fa fa-bar-chart-o"></i> Orders List <span class="fa fa-chevron-down"></span></a>
                  </li>
                  @endif
              </ul>
            </div>
            <div class="menu_section">
                <br>
              <h3>Others</h3>
              <br>
              <ul class="nav side-menu">
            </li>
            <li><form method="post" action="{{ route('logout') }}">@CSRF<span><button id="logout" type="submit" class="btn-block btn" style="background-color:#E23636"><span style="color:white"><i class="fa fa-circle-o text-red " style="margin-right:3%"></i>Logout</span></button></span></form>
              </ul>
            </div>

          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            {{-- <a data-toggle="tooltip" data-placement="top" title="Logout"> --}}
            <form action="{{ route('logout') }}" method="post"> @method('Delete') @CSRF <button class="btn btn-block" type="submit">Logout</button></form>
            {{-- </a> --}}
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="{{ asset('/user_img/'.Auth::user()->img) }}" alt="">{{ Auth::user()->fname }} {{ Auth::user()->lname}}
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li><a href="{{route('employee.show',Auth::user()->id)}}">Account Settings</a>
                  </li>
                <li><a> <form action="{{ route('logout') }}" method="post"> @CSRF <button class="btn-block btn" type="submit">Logout<i class="fa fa-sign-out pull-right"></i></button></form></a>
                  </li>
                </ul>
              </li>

              <li role="presentation" class="dropdown">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-envelope-o"></i>
                  <span class="badge bg-green">6</span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                  <li>
                    <a>
                      <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <div class="text-center">
                      <a href="inbox.html">
                        <strong>See All Alerts</strong>
                        <i class="fa fa-angle-right"></i>
                      </a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->


      <!-- page content -->
      <div class="right_col" role="main" style="background-color:white">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
             @if(Route::current()->getName() == 'admin.dashboard')
             <h1> Dashboard </h1>
             <ol class="breadcrumb">
                    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <hr>
                  </ol>
             @elseif(Route::current()->getName() == 'employee.create')
             <h1> Add User </h1>
             <hr>
             <ol class="breadcrumb">
                    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="{{ route('employee.index') }}"><i class="fa fa-search"></i> View User</a></li>

                    <li class="active">Add User</li>
                  </ol>
             @elseif(Route::current()->getName() == 'employee.index')
             <h1> List User </h1>
             <hr>
             <ol class="breadcrumb">
                    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="{{ route('employee.create') }}"><i class="fa fa-plus"></i> Add User</a></li>
                    <li class="active">List User</li>
                    </ol>
             @elseif(Route::current()->getName() == 'category.index')
             <h1> List Categories </h1>
             <hr>
             <ol class="breadcrumb">
                    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">List Categories</li>
                    </ol>
            @elseif(Route::current()->getName() == 'meal.create')
            <h1> Add Meals </h1>
            <hr>
            <ol class="breadcrumb">
                     <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                     <li><a href="{{ route('meal.index') }}"><i class="fa fa-search"></i> View Meals</a></li>
                     <li><a href="{{ route('tea.index') }}"><i class="fa fa-search"></i>View Tea</a></li>
                     <li class="active">Add Meals</li>
                     </ol>
            @elseif(Route::current()->getName() == 'meal.index')
            <h1> View Meals </h1>
            <hr>
            <ol class="breadcrumb">
                     <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                     <li><a href="{{ route('meal.create') }}"><i class="fa fa-plus"></i> Add Meal Schedule</a></li>
                     <li class="active">View Meals</li>
                     </ol>
            @elseif(Route::current()->getName() == 'product.create')
             <h1> Add Products </h1>
             <hr>
             <ol class="breadcrumb">
                     <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                     <li><a href="{{ route('product.index') }}"><i class="fa fa-search"></i>View Products</a></li>
                     <li class="active">Add Products</li>
                     </ol>
            @elseif(Route::current()->getName() == 'product.index')
            <h1> View Products </h1>
            <hr>
            <ol class="breadcrumb">
                    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="{{ route('product.create') }}"><i class="fa fa-plus"></i> Add New Product</a></li>
                    <li class="active">View Products</li>
                    </ol>
            @elseif(Route::current()->getName() == 'productcat.index')
            <h1> View Categories </h1>
            <hr>
            <ol class="breadcrumb">
                    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">View Categories</li>
                    </ol>
             @elseif(Route::current()->getName() == 'poduct.show')
             <h1> Transfer Products </h1>
             <hr>
             <ol class="breadcrumb">
                     <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="{{ route('product.index') }}"><i class="fa fa-search"></i> View Products</a></li>
                     <li class="active">Transfer Products</li>
                     </ol>
              @elseif(Route::current()->getName() == 'poduct.edit')
              <h1> Edit Products </h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li class="active"> Edit Products </li>
                      </ol>
              @elseif(Route::current()->getName() == 'inventory.index')
              <h1> List Inventory </h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li><a href="{{ route('product.index') }}"><i class="fa fa-plus"></i> Add Inventory</a></li>
                      <li class="active">List Inventory</li>
                      </ol>
              @elseif(Route::current()->getName() == 'dish.create')
              <h1>Add New Dish</h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li><a href="{{ route('dish.index') }}"><i class="fa fa-search"></i> View Dish</a></li>
                      <li class="active">Add Dish</li>
                      </ol>
              @elseif(Route::current()->getName() == 'dish.index')
              <h1>View Dish</h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li><a href="{{ route('dish.create') }}"><i class="fa fa-plus"></i> Add Dish</a></li>
                      <li class="active">View Dish</li>
                      </ol>
              @elseif(Route::current()->getName() == 'dish.edit')
              <h1>Edit Dish</h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li><a href="{{ route('dish.index') }}"><i class="fa fa-search"></i> View Dish</a></li>
                      <li class="active">Edit Dish</li>
                      </ol>
              @elseif(Route::current()->getName() == 'product.edit')
              <h1>Edit Product</h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li><a href="{{ route('product.index') }}"><i class="fa fa-search"></i> View products</a></li>
                      <li class="active">Edit Product</li>
                      </ol>
              @elseif(Route::current()->getName() == 'tea.index')
              <h1>View Tea</h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li><a href="{{ route('meal.create') }}"><i class="fa fa-search"></i> Add Tea Schedule</a></li>
                      <li class="active">View Tea</li>
                      </ol>
              @elseif(Route::current()->getName() == 'meal.edit')
              <h1>Edit Meal</h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li><a href="{{ route('Meal.Index') }}"><i class="fa fa-search"></i> View Meal Schedule</a></li>
                      <li class="active">Edit Meal</li>
                      </ol>
              @elseif(Route::current()->getName() == 'tea.edit')
              <h1>Edit Tea</h1>
              <hr>
              <ol class="breadcrumb">
                      <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li><a href="{{ route('tea.index') }}"><i class="fa fa-search"></i> View Tea Schedule</a></li>
                      <li class="active">Edit Tea</li>
                      </ol>
              @endif
              @yield('section')

            </section>
            <div class="row">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                       @if(Session::has('alert-' . $msg))
                           <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                           </p>
                       @endif
                    @endforeach
                 </div>
            @yield('content')
        <!-- footer content -->

        <div class="clearfix"></div>
        {{-- <footer>
          <div class="copyright-info">
            <p class="pull-right">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer> --}}
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    </div>



  <script src="{{asset('js/bootstrap.min.js')}}"></script>

  <!-- gauge js -->
  <script type="text/javascript" src="{{asset('js/gauge/gauge.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/gauge/gauge_demo.js')}}"></script>
  <!-- bootstrap progress js -->
  <script src="{{asset('js/progressbar/bootstrap-progressbar.min.js')}}"></script>
  <script src="{{asset('js/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <!-- icheck -->
  <script src="{{asset('js/icheck/icheck.min.js')}}"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="{{asset('js/moment/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/datepicker/daterangepicker.js')}}"></script>
  <!-- chart js -->
  <script src="{{asset('js/chartjs/chart.min.js')}}"></script>

  <script src="{{asset('js/custom.js')}}"></script>

  <!-- flot js -->
  <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
  <script type="text/javascript" src="{{asset('js/flot/jquery.flot.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/flot/jquery.flot.pie.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/flot/jquery.flot.orderBars.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/flot/jquery.flot.time.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/flot/date.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/flot/jquery.flot.spline.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/flot/jquery.flot.stack.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/flot/curvedLines.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/flot/jquery.flot.resize.js')}}"></script>

  <!-- worldmap -->
  <script type="text/javascript" src="{{asset('js/maps/jquery-jvectormap-2.0.3.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/maps/gdp-data.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/maps/jquery-jvectormap-world-mill-en.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/maps/jquery-jvectormap-us-aea-en.js')}}"></script>
  <!-- pace -->
  <script src="{{asset('js/pace/pace.min.js')}}"></script>
  @yield('footer_scripts')

  <!-- skycons -->
  <script src="{{asset('js/skycons/skycons.min.js')}}"></script>
<script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>

  <!-- /dashbord linegraph -->
  <!-- datepicker -->
  <script type="text/javascript">
    $(document).ready(function() {

      var cb = function(start, end, label) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      var optionSet1 = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2019',
        maxDate: '12/31/2050',
        dateLimit: {
          days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           },
        opens: 'right',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM-DD-YYYY',
        separator: ' to ',
        locale: {
          applyLabel: 'Submit',
          cancelLabel: 'Clear',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
          monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          firstDay: 1
        }
      };
       $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM-DD-YYYY') + ' - ' + moment().format('MMMM-DD-YYYY'));
      $('#reportrange').daterangepicker(optionSet1, cb);
    });
  </script>
  <script>
    NProgress.done();
  </script>
  <!-- /datepicker -->
  <!-- /footer content -->
</body>

</html>
