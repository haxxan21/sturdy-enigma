<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lava Material - Web Application and Website Multipurpose Admin Panel Template</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="images/fav.ico">

<style>
* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
input {
    line-height: normal;
}
input:not([type]), input[type=text],
input[type=password], input[type=email],
input[type=url], input[type=time],
input[type=date], input[type=datetime],
input[type=datetime-local], input[type=tel],
input[type=number], input[type=search],
textarea.materialize-textarea {
    background-color: transparent;
    border: none;
    border-bottom: 1px solid #9e9e9e;
    border-radius: 0;
    outline: none;
    height: 3rem;
    width: 100%;
    font-size: 1rem;
    margin: 0 0 20px 0;
    padding: 0;
    box-shadow: none;
    box-sizing: content-box;
    transition: all 0.3s;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 400;
}
input:not([type]).validate + label,
input[type=text].validate + label,
input[type=password].validate + label,
input[type=email].validate + label,
input[type=url].validate + label,
input[type=time].validate + label,
input[type=date].validate + label,
input[type=datetime].validate + label,
input[type=datetime-local].validate + label,
input[type=tel].validate + label,
input[type=number].validate + label,
input[type=search].validate + label,
textarea.materialize-textarea.validate + label {
    width: 100%;
    pointer-events: none;
}
.row {
    margin-right: -15px;
    margin-left: -15px;
}
.row:after {
    content: "";
    display: table;
    clear: both;
}
.row .col {
    float: left;
    box-sizing: border-box;
    padding: 0 0.75rem;
    min-height: 1px;
}
.row .col.s12 {
    width: 100%;
    margin-left: auto;
    left: auto;
    right: auto;
}
.input-field {
    position: relative;
    margin-top: 1rem;
}
.input-field label {
    color: #9e9e9e;
    position: absolute;
    top: 0.8rem;
    left: 0;
    font-size: 1rem;
    cursor: text;
    transition: .2s ease-out;
    text-align: initial;
}
.input-field.col label {
    left: 0.75rem;
}
.z-depth-1, nav, .card-panel, .card, .toast,
.btn, .btn-large, .btn-floating, .dropdown-content, .collapsible, .side-nav {
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
}
.btn, .btn-large, .btn-flat {
    border: none;
    border-radius: 2px;
    display: inline-block;
    height: 36px;
    line-height: 36px;
    padding: 0 2rem;
    text-transform: uppercase;
    vertical-align: middle;
    -webkit-tap-highlight-color: transparent;
}
.btn, .btn-large, .btn-floating, .btn-large, .btn-flat {
    font-size: 1rem;
    outline: 0;
}
.btn, .btn-large {
    text-decoration: none;
    text-align: center;
    letter-spacing: .5px;
    transition: .2s ease-out;
    cursor: pointer;
    color: #fff;
    background: #0e76a8;
    font-weight: 600;
}
.btn-large {
    height: 54px;
    line-height: 54px;
}
.waves-effect {
    position: relative;
    cursor: pointer;
    display: inline-block;
    overflow: hidden;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    vertical-align: middle;
    z-index: 1;
    transition: .3s ease-out;
}
.blog-login {
    background: url(http://127.0.0.1:8000/canteen.jpg) no-repeat;
    background-size: cover;
    width: 100%;
    height: 100%;
    position: absolute;
}
.blog-login:before {}
.blog-login-in {
    position: relative;
    width: 30%;
    margin: 0 auto;
    margin-top: 7%;
    background: #fff;
    padding: 40px;
    border-radius: 3px;
    box-shadow: 0px 0px 50px 2px rgba(0, 0, 0, 0.51);
}
.blog-login-in form {
    position: relative;
}
.blog-login-in form img {
    margin: 0 auto;
    display: table;
    padding-bottom: 25px;
}
.blog-login-in input {}
.blog-login-in button {
    color: #fff;
    background-color: #fff;
    border-color: #428433;
    background: #4e923f;
    font-weight: 700;
}
.blog-login-in a {
    display: block;
}
.for-pass {
    text-align: right;
    padding-top: 15px;
    font-size: 15px;
    font-weight: 700;
    color: #0e76a8;
    text-decoration: none;
}
</style>
</head>

<body>
    <div class="blog-login">
        <div class="blog-login-in">
        <form method="POST" action="{{ route('loggingin') }}">
            @CSRF
        {{-- <img src="{{ asset('canteen.jpg') }}" alt="" width="80%" height="200%"/> --}}
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" name="email" class="validate">
                        <label for="email">User Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="last_name" type="password" name="password" class="validate">
                        <label for="last_name">Password</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button class="waves-effect waves-light btn-large btn-log-in" type="submit">Login</button>
                    </div>
                </div>
                <a href="{{ route('password.request') }}" class="for-pass">Forgot Password?</a>
            </form>
        </div>
    </div>

    <!--======== SCRIPT FILES =========-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/travelz/admin/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Dec 2018 05:51:07 GMT -->
</html>
