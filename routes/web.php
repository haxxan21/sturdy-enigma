<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login' ,' Auth\LoginController@showLoginForm')->name('login');
Route::post('/loggingin' , 'Auth\LoginController@attemptLogin')->name('loggingin');
route::post('/logout' , 'Auth\LoginController@logout')->name('logout');

// Extra Routes for web application to export Excel and PDF for meal schedule tea schedule and
// Inventory listing

Route::get('/meal/export', 'Admin\Employee\ExtraController@exportMeal')->name('export.meal');
Route::get('/export/meal' , 'Admin\Employee\ExtraController@exportExcel')->name('export.excel');
Route::get('/export/tea' , 'Admin\Employee\ExtraController@exportTea')->name('tea.pdf');
Route::get('tea/export', 'Admin\Employee\ExtraController@ExportTeaExcel')->name('export.tea.excel');
Route::get('date/{start}/{end}/fetch' , 'Admin\Employee\ExtraController@dateRanger');
Route::get('date/{start}/{end}/fetch/tea' , 'Admin\Employee\ExtraController@dateRangerTea');
Route::get('/export/inventory' , 'Admin\Employee\ExtraController@exportInventoryPDF')->name('inventory.pdf');
Route::get('/inventory/export' , 'Admin\Employee\ExtraController@exportInventoryExcel')->name('inventory.excel');
Auth::routes();

Route::middleware('auth')->group(function () {
Route::get('/', function () {
    return view('Admin.Dashboard');
})->name('admin.dashboard');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where Admin Panel routes are registered. Navigation and operation
| against admin role is performed by these routes.
|
*/
Route::resource('employee', 'Admin\Employee\EmployeeController');
Route::resource('meal', 'Admin\Meals\MealController');
Route::resource('product', 'Admin\Products\ProductController');
Route::resource('productcat', 'Admin\Products\ProductcategoryController')->except(['create']);
Route::resource('inventory', 'Admin\Inventory\InventoryController');
Route::get('dish/view', ['as' => 'dish.index', 'uses' => 'Admin\Dish\DishController@index']);
Route::resource('tea', 'Admin\Meals\TeaController');
Route::resource('dish', 'Admin\Dish\DishController', ['names' => [
    'index' => 'dish.view'
]])->except([
    'index'
]);

});


/*
|--------------------------------------------------------------------------
| Cook Routes
|--------------------------------------------------------------------------
|
| Here is where Cook Panel routes are registered. Navigation and operation
| against cook role is performed by these routes.
|
*/





/*
|--------------------------------------------------------------------------
| System Handler Routes
|--------------------------------------------------------------------------
|
| Here is where Handler Panel routes are registered. Navigation and operation
| against handler role is performed by these routes.
|
*/
