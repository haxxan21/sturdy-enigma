<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'prd_name' , 'prd_ctgry' , 'prd_price', 'prd_qty', 'img'
    ];
}
