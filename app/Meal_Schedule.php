<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal_Schedule extends Model
{
    protected $fillable = [
        'date', 'time', 'dish_name' , 'rotti', 'tea'
    ];
}
