<?php

namespace App\Http\Controllers\Admin\Meals;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\AddMealRequest;
use App\Http\Controllers\Controller;
use App\Dish;
use App\Product;
use App\Meal_Schedule;
class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Meal_Schedule::orderBy('date' , 'DESC')->paginate(15);
        return view('Admin.Meals.View_meal')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = Product::all('prd_name')->toArray();
        $dish = Dish::select('dish_name')
        ->where('dish_name' , '<>' , 'rotti')
        ->where('dish_name' , '<>' , 'tea')
        ->get();
        return view('Admin.Meals.Add_meal')->with(compact('dish'))->with(compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddMealRequest $request)
    {
        $data = $request->all();
        $array_to_string = implode(" , ",$data['dish_name']);
        $data['dish_name'] = $array_to_string;


        if(isset($_POST['add_meal'])){
            $meal = Meal_Schedule::where('date' , '=' , $data['date'])->where('time' , '=' , $data['time'])->where('dish_name' , '=' , $data['dish_name'])->where('rotti' , '=' , '1')->first();
            try {
                if($meal === null){
               Meal_Schedule::create($data);
                session()->flash('alert-success', 'Meal has been added successfully');
                return redirect()->back();
            }else{
                    session()->flash('alert-danger' , 'Meal is already scheduled at this date and time');
                    return redirect()->back();
                }
             }
            catch (\Throwable $th) {

                session()->flash('alert-danger' , $th->getMessage());
                return redirect()->back();

            }
        }

        if(isset($_POST['add_tea'])){
            $meal = Meal_Schedule::where('date' , '=' , $data['date'])->where('time' , '=' , $data['time'])->where('dish_name' , '=' , $data['dish_name'])->where('tea' , '=' , '1')->first();
            try{
                if($meal === null){
                Meal_schedule::create($data);
                session()->flash('alert-success', 'Tea has been added successfully');
                return redirect()->back();
                }else{
                    session()->flash('alert-danger' , 'Tea is already scheduled at this date at time');
                    return redirect()->back();
                }
            }catch(\Throwable $th){
                session()->flash('alert-danger' , 'Tea cannot be added, please try again');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Meal_Schedule::findOrfail($id);
        $dish = Dish::select('dish_name')
        ->where('dish_name' , '<>' , 'rotti')
        ->where('dish_name' , '<>' , 'tea')
        ->get();
        // print_r($data);
        return view('Admin.Meals.Edit_Meal')->with(compact('data'))->with(compact('dish'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $array_to_string = implode(" , ",$data['dish_name']);
        $data['dish_name'] = $array_to_string;

        if(isset($_POST['update_meal'])){
                try{
                Meal_Schedule::where('id' , $id)->update([
                    'date' =>$data['date'],
                    'time' => $data['time'],
                    'dish_name' => $data['dish_name'],
                    'rotti' => $data['rotti']
                ]);
                session()->flash('alert-success' , 'Meal schedule has been updated successfully');
                return redirect()->back();

        }catch( \Throwable $th){

            session()->flash('alert-danger' , 'Meal schedule cannot be updated, please try again');
            return redirect()->back();
        }
        }

        elseif(isset($_POST['add_meal'])){
            try {
                $meal = Meal_Schedule::where('date' , '=' , $data['date'])->where('time' , '=' , $data['time'])->where('dish_name' , '=' , $data['dish_name'])->where('rotti' , '=' , '1')->first();
                if($meal === null){
                $meal = new Meal_Schedule();
                $meal->date = $data['date'];
                $meal->time = $data['time'];
                $meal->dish_name = $data['dish_name'];
                $meal->rotti = $data['rotti'];
                $meal->save();
                session()->flash('alert-success' , 'Meal schedule has been added successfully');
                return redirect()->route('meal.index');

            }else{
                session()->flash('alert-danger' , 'Meal is already scheduled at this date and time');
                return redirect()->back();
            }
        }catch (\Throwable $th) {
                session()->flash('alert-danger' , $th->getMessage());
                return redirect()->back();
            }

    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

        Meal_Schedule::where('id' , $id)->delete();
        session()->flash('alert-success' , 'Meal schedule has been deleted successfully');
        return redirect()->back();

        } catch(\Throwable $th){

            session()->flash('alert-danger' , 'Meal schedule cannot be deleted, please try again');
            return redirect()->back();

        }
    }
}
