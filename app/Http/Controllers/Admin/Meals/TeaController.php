<?php

namespace App\Http\Controllers\Admin\Meals;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Meal_Schedule;
use App\Product;
class TeaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Meal_Schedule::orderBy('date' , 'DESC')->paginate(15);
        return view('Admin.Meals.View_tea')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::all('prd_name')->toArray();
        $data = Meal_Schedule::findOrfail($id);
        return view('Admin.Meals.Edit_Tea')->with(compact('data'))->with(compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $array_to_string = implode(" , ",$data['dish_name']);
        $data['dish_name'] = $array_to_string;
       if(isset($_POST['update_tea'])){
        try {

            Meal_Schedule::where('id' , $id)->update([
                'date' => $data['date'],
                'time' => $data['time'],
                'dish_name' => $data['dish_name'],
                'tea' => $data['tea']
            ]);
            session()->flash('alert-success' , 'Tea schedule has been updated successfully');
            return redirect()->route('tea.index');

        } catch (\Throwable $th) {

            session()->flash('alert-danger', 'Tea schedule cannot be updated, please try again');
            return redirect()->back();

        }
    }
    elseif(isset($_POST['add_tea'])){

        try {
            $meal = Meal_Schedule::where('date' , '=' , $data['date'])->where('time' , '=' , $data['time'])->where('rotti' , '=' , '1')->first();
            if($meal === null ){
            $tea = new Meal_Schedule();
            $tea->date = $data['date'];
            $tea->time = $data['time'];
            $tea->dish_name = $data['dish_name'];
            $tea->tea = $data['tea'];
            $tea->save();

            session()->flash('alert-success' , 'Tea schedule has been added successfully');
            return redirect()->route('tea.index');

        }else{
            session()->flash('alert-danger' , 'Tea is already scheduled at this date and time');
            return redirect()->back();
        }
        }catch (\Throwable $th) {

            session()->flash('alert-danger', 'Tea schedule cannot be added, please try again');
            return redirect()->back();

        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

        Meal_Schedule::where('id' , $id)->delete();
        session()->flash('alert-success', 'Tea schedule has been deleted successfully');
        return redirect()->back();

        } catch (\Throwable $th) {

            session()->falsh('alert-danger' , 'Tea schedule cannot be deleted, please try again');
            return redirect()->back();

        }
    }

}
