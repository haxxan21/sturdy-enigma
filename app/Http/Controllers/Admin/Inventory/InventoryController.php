<?php

namespace App\Http\Controllers\Admin\Inventory;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\UpdateInventoryRequest;
use App\Http\Controllers\Controller;
use App\Product;
use App\Inventory;
use DB;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Inventory::join('products', 'products.id', 'inventories.prd_id')->select(
            DB::raw('inventories.id as id'),
            DB::raw('products.prd_name as products'),
            DB::raw('products.prd_ctgry as category'),
            DB::raw('inventories.quantity as quantity'),
            DB::raw('inventories.date as date')
        )->paginate(15);

        return view('Admin.Inventory.List_inventory')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        try {
            $product = Product::where('id' , $data['prd_id'])->pluck('prd_qty')->toArray();
            if($data['quantity'] < $product[0]){
                $updated_product_quantity = $product[0] - $data['quantity'];
            Product::where('id' , $data['prd_id'])->update([
                        'prd_qty' => $updated_product_quantity
            ]);
            Inventory::create($data);
            session()->flash('alert-success', 'Product has been added to the inventory');
            return redirect()->back();
            }
            else{
                session()->flash('alert-danger' , 'Entered Quantity in greater than available quantity');
                return redirect()->back();
            }
            // Inventory::create($data);
            // session()->flash('alert-success', 'Product has been added to the inventory');
            // return redirect()->route('inventory.index');

        } catch (\Throwable $th) {

            session()->flash('alert-danger' , $th->getMessage());
            return redirect()->back();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Product::findOrfail($id);
        return view('Admin.Inventory.Transfer_products')->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Inventory::findOrfail($id);
        return view('Admin.Inventory.Edit_Inventory')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInventoryRequest $request, $id)
    {
        $data = $request->all();
        try{

            Inventory::where('id' , $id)->update([
                'quantity' => $data['quantity'],
                'date' => $data['date']
            ]);
            session()->flash('alert-success' , 'inventory updated successfully');
            return redirect()->route('inventory.index');

        }catch(\Throwable $th){

            session()->flash('alert-danger' , 'cannot update inventory, please try again');
            return redirect()->back();

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            Inventory::where('id', $id)->delete();
            session()->flash('alert-success' , 'Product has been deleted successfylly');
            return redirect()->back();

        }catch(\Throwable $th){

            session()->flash('alert-danger' , 'Product cannot be deleted, Please try again');
            return redirect()->back();

        }
    }
}
