<?php

namespace App\Http\Controllers\Admin\employee;

use Illuminate\Http\Request;
use App\Meal_Schedule;
use App\Http\Controllers\Controller;
use PDF;
use DB;
use Excel;
use App\Inventory;
class ExtraController extends Controller
{
    /*
    |
    | Exporting Meal Schedule history to PDF (downloaded file)
    |
    */

    public function exportMeal(){

        $datas  = DB::table('meal__schedules')->get();
        $pdf = PDF::loadView('Admin.Meals.Print_meal_schedule', compact('datas'))->setPaper('a4','portrait');
        return $pdf->stream('meals.pdf');

    }

    /*
    |
    | Export Meal schedule History to Excel (xls format downloaded file)
    |
    */

    public function exportExcel(){
        $data = Meal_Schedule::select(
            DB::raw('date as Date'),
            DB::raw('time as Time'),
            DB::raw('dish_name as Dish'),
            DB::raw("(CASE rotti WHEN 1 THEN 'Available' ELSE 'Unavailable' END) as Rotti_Available")
        )
        ->get()
        ->toArray();

        return Excel::create('Meal_Schedule', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('xls');
    }

    /*
    |
    | Export Tea Schedule history to PDF (download file)
    |
    */

    public function exportTea(){

        $datas  = DB::table('meal__schedules')->where('tea' , 1)->get();
        $pdf = PDF::loadView('Admin.Meals.Print_tea_schedule', compact('datas'))->setPaper('a4','portrait');
        return $pdf->stream('tea.pdf');

    }

    /*
    |
    | Export Tea Schedule history to Excel (xls format downloaded file)
    |
    */

    public function exportTeaExcel(){
        $data = Meal_Schedule::select(
            DB::raw('date as Date'),
            DB::raw('time as Time'),
            DB::raw('dish_name as Product')
        )
        ->where('tea' , 1)
        ->get()
        ->toArray();

        return Excel::create('Tea_Schedule', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('xls');
    }

    /*
    |
    | Export Inventory listing to PDF (downloaded file)
    |
    */

    public function exportInventoryPDF(){

        $datas = DB::table('inventories')->join('products', 'products.id', 'inventories.prd_id')->select(
            DB::raw('inventories.id as id'),
            DB::raw('products.prd_name as products'),
            DB::raw('products.prd_ctgry as category'),
            DB::raw('inventories.quantity as quantity'),
            DB::raw('inventories.date as date')
        )->get();
        $pdf = PDF::loadView('Admin.Inventory.Print_inventory_list' , compact('datas'))->setPaper('a4' , 'portrait');
        return $pdf->stream('inventory.pdf');

    }

    /*
    |
    | Export Inventory Listing to Excel file (xls format downloaded file)
    |
    */

    public function exportInventoryExcel(){
        $datas = Inventory::join('products', 'products.id', 'inventories.prd_id')->select(
            DB::raw('inventories.id as Number'),
            DB::raw('products.prd_name as Products'),
            DB::raw('products.prd_ctgry as Category'),
            DB::raw('inventories.quantity as Quantity'),
            DB::raw('inventories.date as Date')
        )
        ->get()
        ->toArray();
        return Excel::create('Inventory_history', function($excel) use ($datas) {
            $excel->sheet('mySheets' , function($sheet) use ($datas){
                $sheet->fromArray($datas);
            });
        })->download('xls');
    }

    /**
     * Function to handle AJAX call from view.meal.schedule JQuery DateRangepicker
     *    @param string $start where start date is passed
     *    @param string $end where end date is passed
     *    return response objects
    */

    public function dateRanger($start, $end){
        $time_1 = strtotime($start);
        $time_2 = strtotime($end);
        $start_date_format = date('Y-d-m' , $time_1);
        $end_date_format = date('Y-d-m' , $time_2);
        // $dates = $start_date_format .'-'. $end_date_format;
        $data = Meal_Schedule::select(
            DB::raw('date as Dates'),
            DB::raw('time as Time'),
            DB::raw('dish_name as Dish'),
            DB::raw("(CASE rotti WHEN 1 THEN 'Available' ELSE 'Unavailable' END) as Rotti_Available")
        )
        ->whereBetween('date', [$start_date_format, $end_date_format])
        ->where('tea' , NULL)
        ->get()
        ->toArray();
        return response(array(
            'meals' => $data,
        ));
    }

    /**
     *
     * Function to handle AJAX calls from view.tea.schedule JQuery DateRangePicker
     * @param string $start where start date is passed to the function
     * @param string @end where end date is passed down to the function
     * return response object
     *
    */

    public function dateRangerTea($start, $end){
        $time_1 = strtotime($start);
        $time_2 = strtotime($end);
        $start_date_format = date('Y-d-m' , $time_1);
        $end_date_format = date('Y-d-m' , $time_2);
        // $dates = $start_date_format .'-'. $end_date_format;
        $data = Meal_Schedule::select(
            DB::raw('date as Dates'),
            DB::raw('time as Time'),
            DB::raw('dish_name as Dish')
        )
        ->whereBetween('date', [$start_date_format, $end_date_format])
        ->where('rotti' , NULL)
        ->get()
        ->toArray();
        return response(array(
            'meals' => $data,
        ));
    }
}
