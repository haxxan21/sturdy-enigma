<?php

namespace App\Http\Controllers\Admin\Employee;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\AddUserRequest;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::whereNotNull('role')->paginate(15);
        return view('Admin.Employees.List_employee')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Employees.Add_Employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUserRequest $request)
    {
        $data = $request->all();
        $user = User::where('email' , '=' , $data['email'])->first();
        if($user === null){
        try {
            $employee_id_get = date('m/d/Y h:i:s a', time());
            // hashing the time
            $employee_id_hash = md5($employee_id_get);
            // trimming the string to 6 letter ( 32-26 = 6) substring;
            $employee_id = substr($employee_id_hash, 0, -26);

            $user = new User();
            $user->fname = $data['fname'];
            $user->lname = $data['lname'];
            $user->mobile = $data['mobile'];
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->role = $data['role'];
            $user->status = $data['status'];
            $user->emp_num = $employee_id;
            $user->save();
            session()->flash('alert-success', 'User Added Successfully');
            return redirect()->route('employee.index');

        } catch (\Throwable $th) {

            session()->flash('alert-danger' , $th->getMessage());
            return redirect()->back();
        }
    }
        else{
            session()->flash('alert-danger' , 'User Email Already Exists');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return view('Admin.Employees.profile');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrfail($id);
        return view('Admin.Employees.Edit_employee')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if(isset($_POST['update_profile'])){
            // checking if request has image
            if($request->hasFile('img')) {
                $file = $request->file('img');
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $imgname = uniqid().$filename;
                $destinationPath = public_path('/user_img/');
                $file -> move($destinationPath , $imgname);
              }
            try {
                //if user submits form with image and password
                if(!empty($data['img']) && !empty($data['password'])){
                User::where('id', $id)->update([
                    'password' => Hash::make($data['password']),
                    'img' => $imgname,
                ]);
                session()->flash('alert-success', 'User Updated successfully');
                return redirect()->back();
                }
                //if user submits form without image
                elseif(!empty($data['password']) && empty($data['img'])){
                    User::where('id', $id)->update([
                        'password' => Hash::make($data['password'])
                    ]);
                        session()->flash('alert-success', 'Password Updated successfully');
                        return redirect()->back();

                }
                //if user submits form without password
                elseif(empty($data['password']) && !empty($data['img'])){
                    User::where('id', $id)->update([
                        'img' => $imgname,
                    ]);
                        session()->flash('alert-success', 'Profile picture Updated successfully');
                        return redirect()->back();
                }
                //if users tries to submit the form without any value
                else{
                        session()->flash('alert-warning' , 'Enter Values to Update Profile');
                        return redirect()->back();
                }

            } catch (\Throwable $th) {
                session()->falsh('alert-danger' , 'User cannot be updated, please try again');
                return redirect()->back();
            }
        }

        else{
        try {

            User::where('id', $id)->update([
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'mobile' => $data['mobile'],
                'role' => $data['role'],
                'status' => $data['status'],
                'email' => $data['email']
            ]);
                session()->flash('alert-success', 'User Updated successfully');
                return redirect()->route('employee.index');

        } catch (\Throwable $th) {
                session()->flash('alert-danger' , 'User cannot be updated, please try again');
                return redirect()->back();
        }
    }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            User::where('id' , $id)->delete();
            session()->flash('alert-success' , 'User has been deleted successfully');
            return redirect()->back();

        } catch (\Throwable $th) {

            session()->flash('alert-danger' , 'User cannot be deleted, please try again');
            return redirect()->back();

        }
    }
}
