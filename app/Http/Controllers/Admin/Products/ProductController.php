<?php

namespace App\Http\Controllers\Admin\Products;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\AddProductRequest;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Product::paginate(10);
        return view('Admin.products.View_products')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Category::get()->toArray();
        return view('Admin.products.Add_product')->with(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddProductRequest $request)
    {
        $data = $request->all();
        //checking if submit request has file
        if($request->hasFile('img')) {
            $file = $request->file('img');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $imgname = uniqid().$filename;
            $destinationPath = public_path('/prd_img/');
            $file -> move($destinationPath , $imgname);
          }
          $product = Product::where('prd_name', '=', $data['prd_name'])->first();
            if ($product === null) {
                // user doesn't exist
          try {
              //if submit request has image
            if(!empty($data['img'])){
            $product = new Product();
            $product->prd_name = $data['prd_name'];
            $product->prd_ctgry = $data['prd_ctgry'];
            $product->prd_price = $data['prd_price'];
            $product->prd_qty = $data['prd_qty'];
            $product->img = $imgname;
            $product->save();
            }
            //if request is submitted without image
            else{
            $product = new Product();
            $product->prd_name = $data['prd_name'];
            $product->prd_ctgry = $data['prd_ctgry'];
            $product->prd_price = $data['prd_price'];
            $product->prd_qty = $data['prd_qty'];
            $product->save();
            }
         session()->flash('alert-success' , 'product has been added successfully');
         return redirect()->back();

        } catch (\Throwable $th) {
            session()->flash('alert-danger' , $th->getMessage());
            return redirect()->back();
          }
        }
        else{
            session()->flash('alert-danger' , 'Product Already Exists');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return view('Admin.products.Transfer_products');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Product::findOrfail($id);
        $category = Category::select('category_name');
        // print_r($category);
       return view('Admin.products.Edit_product')->with(compact('data'))->with(compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($request->hasFile('img')) {
            $file = $request->file('img');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $imgname = uniqid().$filename;
            $destinationPath = public_path('/prd_img/');
            $file -> move($destinationPath , $imgname);
          }
          try {
            if(!empty($data['img'])){
            Product::where('id', $id)->update(['prd_name'=>$data['prd_name'],
                                      'prd_ctgry'=>$data['prd_ctgry'],
                                      'prd_price' =>$data['prd_price'],
                                      'prd_qty' => $data['prd_qty'],
                                      'img'=> $imgname
            ]);
            }
            else{
                Product::where('id', $id)->update(['prd_name'=>$data['prd_name'],
                                      'prd_ctgry'=>$data['prd_ctgry'],
                                      'prd_qty' => $data['prd_qty'],
                                      'prd_price' =>$data['prd_price'],
                ]);
            }
         session()->flash('alert-success' , 'product has been updated successfully');
         return redirect()->route('product.index');

        } catch (\Throwable $th) {
            session()->flash('alert-danger' ,  $th->getMessage());
            return redirect()->back();
          }
        }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            Product::where('id', $id)->delete();
            session()->flash('alert-success' , 'product deleed successfully');
            return redirect()->back();

        } catch (\Throwable $th) {

            session()->flash('alert-danger', 'Product cannot be deleted, please try again');
            return redirect()->back();

        }
    }
}
