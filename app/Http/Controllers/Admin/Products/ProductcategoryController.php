<?php

namespace App\Http\Controllers\Admin\Products;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\AddProductcatRequest;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use DB;
class ProductcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Product::Join('categories' , 'products.prd_ctgry' , 'categories.category_name')->select(
            DB::raw('DISTINCT(products.prd_ctgry) as category'),
            DB::raw('COUNT(products.prd_name) as products'),
            DB::raw('categories.created_at as date')
        )->groupBy('products.prd_ctgry')->groupBy('categories.created_at')->get()->toArray();;

        return view('Admin.products.List_category')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddProductcatRequest $request)
    {
        try {

        Category::create($request->all());
        session()->flash('alert-success' , 'product category has been added successfully');
        return redirect()->back();

        } catch (\Throwable $th) {

            session()->flash('alert-danger', 'product category cannot be added, please try again');
            return redirect()->back();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // echo $id;
        $ctgry_name = Category::where('category_name' , $id)->get();
        return response(array(
            'ctgry' => $ctgry_name
        ));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $id = $data['id'];

        try {

            Category::where('id', $id)->update([
                'category_name' => $data['category_name']
                ]);
            session()->flash('alert-success' , 'Category Updated Successfully');
            return redirect()->back();

        } catch (\Throwable $th) {

            session()->flash('alert-danger' , 'Cannot Update product category, please try again later');
            return redirect()->back();

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            Category::where('category_name', $id)->delete();
            session()->flash('alert-success' , 'Category has been deleted successfully');
            return redirect()->back();

        } catch (\Throwable $th) {

            session()->flash('alert-danger' , 'Category cannot be deleted, please try again');
            return redirect()->back();

        }
    }
}
