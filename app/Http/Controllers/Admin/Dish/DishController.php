<?php

namespace App\Http\Controllers\Admin\Dish;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\AddDishRequest;
use App\http\Requests\Admin\UpdateDishRequest;
use App\Http\Controllers\Controller;
use App\Dish;
class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Dish::paginate(10);
        return view('Admin.Dishes.View_dish')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Dishes.Add_dish');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddDishRequest $request)
    {

        $data = $request->all();

        // checking if submit request has file

        if($request->hasFile('img')) {
            $file = $request->file('img');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $imgname = uniqid().$filename;
            $destinationPath = public_path('/dish_img/');
            $file -> move($destinationPath , $imgname);
          }
          // if successful

          try {
            $dish = new Dish();
            $dish->dish_name = $data['dish_name'];
            $dish->dish_price = $data['dish_price'];
            $dish->img = $imgname;
            $dish->save();
         session()->flash('alert-success' , 'dish has been added successfully');
         return redirect()->back();

        } // if failed
        catch (\Throwable $th) {
            session()->flash('alert-danger' , 'Cannot add dish please try again');
            return redirect()->back();
          }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data = Dish::findOrfail($id);
       return view('Admin.Dishes.edit_dish')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDishRequest $request, $id)
    {
        $data = $request->all();

        // Checking if submit request has file

        if($request->hasFile('img')) {
            $file = $request->file('img');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $imgname = uniqid().$filename;
            $destinationPath = public_path('/dish_img/');
            $file -> move($destinationPath , $imgname);
          }
          try {
              // if dish name is not empty
            if(!empty($data['img'])){
            Dish::where('id', $id)->update(['dish_name'=>$data['dish_name'],
                                      'dish_price'=>$data['dish_price'],
                                      'img'=> $imgname
            ]);
            }

            //if dish is empty

            else{
                Dish::where('id', $id)->update(['dish_name'=>$data['dish_name'],
                                      'dish_price'=>$data['dish_price']
                ]);
            }
         session()->flash('alert-success' , 'dish has been updated successfully');
         return redirect()->route('dish.index');

        } catch (\Throwable $th) {
            session()->flash('alert-danger' ,  $th->getMessage());
            return redirect()->back();
          }
        }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            Dish::where('id', $id)->delete();
            session()->flash('alert-success' , 'Dish has been deleted successfully');
            return redirect()->back();

        } catch (\Throwable $th) {

            session()->flash('alert-danger' , 'Dish cannot be deleted. Please try again');
            return redirect()->back();

        }
    }
}
