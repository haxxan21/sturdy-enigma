<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
      return view('login');
    }

    protected function credentials(\Illuminate\Http\Request $request)
    {
        //return $request->only($this->username(), 'password');
        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status' => 'active'];
    }

    /* overriding the function attemptLogin() from Illuminate\Foundation\Auth\AuthenticatesUsers
    |--to authenticate and redirect to the desired path
    */

    protected function attemptLogin(Request $request)
    {
          // code...
          if ( $this->guard()->attempt(
                  $this->credentials($request), $request->filled('remember')));
          {
            return redirect()->route('admin.dashboard');
          }

     }

     /* overriding the function logout() from Illuminate\Foundation\Auth\AuthenticatesUsers
     |--to flush the running session and redirect to the login page
     */

    public function logout(Request $request){
      $this->guard()->logout();
      return redirect()->route('login');
    }

    /* writing specific guard for authentication of the admins
    |-- the guards are defined in Config/auth.php file
    */

}
