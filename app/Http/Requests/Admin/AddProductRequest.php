<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prd_name' => ['required', 'alpha' ,'min:3', 'max:20', 'unique:products'],
            'prd_ctgry' => ['required'],
            'prd_price' => ['required', 'numeric'],
            'prd_qty' => ['required' , 'numeric' ],
            'img' => ['bail','nullable','image:jpeg,png,jpg','max:1048']
        ];
    }
}
