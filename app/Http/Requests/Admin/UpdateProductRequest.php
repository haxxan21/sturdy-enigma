<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prd_name' => ['alpha' , 'min:3' , 'max:50'],
            'prd_ctgry' => ['alpha' , 'min:3' , 'max:50'],
            'prd_price' => ['numeric'],
            'prd_qty' => ['numeric'],
            'img' => ['bail' , 'nullable' , 'image:jpeg,png,jpg' , 'max:1048']
        ];
    }
}
