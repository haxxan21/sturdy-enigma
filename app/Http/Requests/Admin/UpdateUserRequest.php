<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => ['alpha' , 'min:3' , 'max:50'],
            'lname' => ['alpha' , 'min:3' , 'max:50'],
            'mobile' => ['numeric' , 'digits:11' ],
            'email' => ['email']
        ];
    }
}
