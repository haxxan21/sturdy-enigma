<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'dish_name' => ['nullable' , 'alpha' , 'min:2' , 'max:20'],
           'dish_price' => ['nullable' , 'numeric' , 'min:1'],
           'img' => ['bail','nullable','image:jpeg,png,jpg','max:1048'],
        ];
    }
}
