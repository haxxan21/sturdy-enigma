<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'fname' => ['required' , 'alpha' , 'min:3' , 'max:20'],
            'lname' => ['required' , 'alpha' , 'min:3' , 'max:20' ],
            'mobile' => ['nullable' , 'unique:users' , 'numeric' , 'digits:11'],
            'email' => ['nullable' , 'unique:users' , 'email'],
            'status' => ['required'],
            'password' => ['nullable' , 'required_with:con_pass', 'min:6' , 'max:20' , 'string'],
            'con_pass' => ['nullable' , 'required_with:con_pass','same:password']
        ];
    }
}
